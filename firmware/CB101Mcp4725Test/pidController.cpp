#include "PidController.h"

PidController::PidController(float h, float hd, float kp, float ki, float kd, float vMax, float vs)
{
    Hd = Hd;
    HPrev = h;
    
    VMax = vMax;
    Vs = vs;

    Ki = Kp;
    Ki = Ki;
    Kd = Kd;

    ErrorIntegral = 0;
    ERate = 0;
}


float PidController::propotionalVoltage()
{
    return Kp*E;
}

float PidController::integralVoltage()
{
    return Ki*ErrorIntegral;
}

float PidController::derivativeVoltage()
{
    return Kd*ERate;
}

float PidController::update(float h, float t, float dt)
{
    // Update error
    E = Hd - h;    

    // Error Rate
    ERate = (h-HPrev)/dt;

    // Forward-Euler Integral
    ErrorIntegral += E*dt;

    // PID Equation
    float pidVoltage = 
        propotionalVoltage() + 
        integralVoltage() + 
        derivativeVoltage()
    ;

    // Output Voltage
    float voltage = Vs + pidVoltage;

    // Saturator
    if(voltage > VMax)
    {
        voltage = VMax;
        ErrorIntegral -= E*dt;
    }
    else if(voltage < 0)
    {
        voltage = 0;
        ErrorIntegral -= E*dt;
    }

    // Prepare for next iteration
    HPrev = h;

    return voltage;
}
