close all
root = '/Users/aaron/Documents/DLZ/flexStiffnessTest/';
imageFilepaths = {
    [root '0g.JPG']
    [root '5g.JPG']
    [root '10g.JPG']
    [root '15g.JPG']
    [root '20g.JPG']
};
weights = 0:5:20;

% Extract height from each image
heights = [];
for i=1:length(imageFilepaths)
    img = imread(imageFilepaths{i});
    heights(i) = detect(img);
end

% Stuff
bestFit = polyfit(weights, heights, 1);

hold on
grid on
plot(weights, heights)
plot(weights, polyval(bestFit, weights))
legend('data', 'best fit')

xlabel('weight (g)')
ylabel('deflection (mm)')