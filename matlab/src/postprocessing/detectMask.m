function mask = detectMask(hsv, hue_range, sat_range, value_range)
    h = hsv(:,:,1);
    s = hsv(:,:,2);
    v = hsv(:,:,3);
    
    if hue_range(1) > hue_range(2)
        h_mask = h>hue_range(1) | h<hue_range(2);
    else
        h_mask = h>hue_range(1) & h<hue_range(2);
    end
    
    s_mask = sat_range(1) < s &  s <sat_range(2);
    
    v_mask = value_range(1) < v & v < value_range(2);

    mask = h_mask & s_mask & v_mask;
end