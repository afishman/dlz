classdef PidController < handle
    properties
        hd             % Desired Height
        Ki             % Integral Gain
        Kp             % Proportional Gain
        Kd             % Derivative Gain
        hPrev          % Previous height (to calculate height velocity)
        errorIntegral  % Error Integral
        vMax           % Maximum Voltage
        ERate          % Error Rate
        E              % Error
        Vs             % 0 Voltage Level (constant term added to controller output)
        linearise      % Flag indicating whether to scale voltage by height
    end
        
    methods
        function obj = PidController(session, Hd, Kp, Ki, Kd, VMax, Vs, linearise)
            %% Class Constructor
            % Object parameters
            obj.hd = Hd;
            obj.Ki = Ki;
            obj.Kp = Kp;
            obj.Kd = Kd;
            obj.errorIntegral = 0;
            obj.vMax = VMax;
            
            % Set Vs if defined
            if exist('Vs')
                obj.Vs = Vs;
            else
                obj.Vs = 0;
            end
            
            % Set Vs if defined
            if exist('linearise')
                obj.linearise = linearise;
            else
                obj.linearise = false;
            end
          
            % Get current height
            [inputDataLine, triggerTime] = inputSingleScan(session.highVoltageControlSession);    
            currentState = processRawData(session, 0, inputDataLine);
            
            % Inititialise previous height
            obj.hPrev = currentState.height;
        end
        
        function voltage = propotionalVoltage(obj)
            voltage = obj.Kp*obj.E;
        end
        
        function voltage = integralVoltage(obj)
            voltage = obj.Ki*obj.errorIntegral;
        end
        
        function voltage = derivativeVoltage(obj)
            voltage = obj.Kd*obj.ERate;
        end
        
        function hd = currentHd(obj, t)
            if strcmp(class(obj.hd), 'function_handle')
                hd = obj.hd(t);
            else
                hd = obj.hd;
            end     
        end
        
        function voltage = update(obj, h, t, dt)
            %% Update the controller      
            
            % Calculate error and error rate
            % TODO: Make this a function
            hd = obj.currentHd(t);
            
            obj.E = hd-h;
            obj.ERate = (h - obj.hPrev)/dt; %TODO: Make this a function
            
            % Update error integral with forward-euler
            obj.errorIntegral = obj.errorIntegral + (obj.E*dt);
                     
            % PID control law
            pidVoltage = obj.propotionalVoltage + obj.integralVoltage + obj.derivativeVoltage;
            if obj.linearise
               pidVoltage = -pidVoltage/h;
            end

            voltage = obj.Vs + pidVoltage;
            
            % Saturator, stop windup when saturated
            if voltage > obj.vMax
                voltage = obj.vMax;
                obj.errorIntegral = obj.errorIntegral - obj.E*dt;
            elseif voltage < 0
                voltage = 0;
                obj.errorIntegral = obj.errorIntegral - obj.E*dt;
            end
            
            % For next iteration
            obj.hPrev = h;
            
            % For debugging
            %disp({'int', int_new})
            %disp({'voltage', voltage})
        end
        
        function fun = updateFun(obj)
            %% Pass the return of this function to trial
            fun = @(t,h,v,dt) obj.update(h,dt);
        end
    end
    
end