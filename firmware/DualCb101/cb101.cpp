#include "cb101.h"


Cb101::Cb101(int address, int voltageMonitorPin, int currentMonitorPin)
{
    // Setup DAC
    Dac.begin(address);

    // Monitor Pins
    vmPin = voltageMonitorPin;
    cmPin = currentMonitorPin;
}

float Cb101::readVoltageMonitor()
{
    return (highVoltagePsuOutputVolts/1023.0) * ((float)analogRead(vmPin));
}

float Cb101::readCurrentMonitor()
{
    return (highVoltagePsuOutputAmps/1023.0) * ((float)analogRead(cmPin));
}

// Set the high voltage. Saturates at 0 and HIGH_VOLTAGE_PSU_OUTPUT
void Cb101::setHv(float voltage)
{    
    uint16_t dacLevel = 4095*constrain(voltage/highVoltagePsuOutputVolts, 0, 1);

    // Update dac
    Dac.setVoltage(dacLevel, false);
}
