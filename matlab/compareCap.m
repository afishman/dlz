e0 = 8.85418782e-12;
e = 3;

capPred = [];
for i=1:length(C)
    h = hInf(i);
    
    x = linspace(0, beam.L, 100);
    capPred(i) = trapz(capFun(x, beam, -(h-max(hInf))), 0, beam.L);
end

close all
hold on
plot(hInf, capPred)