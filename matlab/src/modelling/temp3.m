% Thanks: https://www.amesweb.info/Vibration/Natural-Frequency-Uniform-Beam-Both-Ends-Fixed.aspx

m = 0.0234;
w = 0.044;
L = 0.1;

fPointLoad = @(m, L, EI) (13.86/(2*pi)) * sqrt( (EI*g)/(m*L^3) );
fUniformLoad = @(w, L, EI) (22.4/(2*pi)) * sqrt( (EI*g)/(w*L^4) );
fUniformPointLoad = @(w, m, L, EI) (13.86/(2*pi)) * sqrt( (EI*g)/(m*L^3 + 0.383*w*L^4) );

fprintf('Point Load freq (eff): %.3f\n', fPointLoad(m, L, EI_eff));
fprintf('Point Load freq (pred): %.3f\n', fPointLoad(m, L, EI_pred));
fprintf('*******\n')

fprintf('Uniform Load freq (eff): %.3f\n', fUniformLoad(w, L, EI_eff));
fprintf('Uniform Load freq (pred): %.3f\n', fUniformLoad(w, L, EI_pred));
fprintf('*******\n')

fprintf('Uniform+Point Load freq (eff): %.3f\n', fUniformPointLoad(w, m, L, EI_eff));
fprintf('Uniform+Point Load freq (pred): %.3f\n', fUniformPointLoad(w, m, L, EI_pred));
fprintf('*******\n')