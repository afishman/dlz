/**************************************************************************/
/*!
    @file     sinewave.pde
    @author   Adafruit Industries
    @license  BSD (see license.txt)

    This example will generate a sine wave with the MCP4725 DAC.

    This is an example sketch for the Adafruit MCP4725 breakout board
    ----> http://www.adafruit.com/products/935

    Adafruit invests time and resources providing this open source code,
    please support Adafruit and open-source hardware by purchasing
    products from Adafruit!
*/
/**************************************************************************/
#include <Wire.h>
#include <Adafruit_MCP4725.h>

Adafruit_MCP4725 dac1;
Adafruit_MCP4725 dac2;

int NUM_INPUT_PINS = 2;
int INPUT_PINS[] = {A4, A9};


void setup(void) {
  Serial.begin(115200);

  // For Adafruit MCP4725A1 the address is 0x62 (default) or 0x63 (ADDR pin tied to VCC)
  // For MCP4725A0 the address is 0x60 or 0x61
  // For MCP4725A2 the address is 0x64 or 0x65
  Serial.println("Beginning");
  dac1.begin(0x62);
  dac2.begin(0x63);

  Serial.println("Generating a sine wave");
}

void loop(void) {
      uint16_t i;


      float t = ((float)millis())/1000;
      float f = 0.2;
      float angle = sin(2*PI*f*t);
      float angleScaled = 4095*( 0.5*(angle+1) );
//      Serial.println(angleScaled);
      uint16_t angleLong = (uint16_t) angleScaled;
      
      dac1.setVoltage(angleLong, false);
      dac2.setVoltage((angleLong+2047)%4095, false);

      String message = "";
      for(int i=0; i<NUM_INPUT_PINS; i++){
          message += String(analogRead(INPUT_PINS[i]));

          if(i!=NUM_INPUT_PINS-1)
          {
            message += ",";
          }
          
      }

      Serial.println(message);
      
      delay(10);
}
