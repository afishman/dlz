clear all
close all

%% A simply supported beam of length L with a uniform load w

L=1;
w=1;
E=1;
I=1;
EI = E*I;

x = linspace(0, L, 100);
y = (1/EI)*( (1/12)*w*L*x.^3 - (1/24)*w*x.^4 - (1/24)*w.*x*L^3  );

plot(x,y)