#include <Adafruit_MCP4725.h>
#include <Wire.h>

class Cb101 
{
    // TODO: MCP4725
    private:
        Adafruit_MCP4725 Dac;

    public:
        Cb101(int addr, int voltageMonitorPin, int currentMonitorPin);
    
        // From CB-101 Datasheet
        // https://www.xppower.com/portals/0/pdfs/SF_CB_Series.pdf
        float const highVoltagePsuOutputVolts = 10000;
        float const highVoltagePsuOutputAmps = 0.001;

        // Monitor Pins
        int vmPin;
        int cmPin;

        float readVoltageMonitor(); // Volts
        float readCurrentMonitor(); // Amps

        void setHv(float voltage);
};
 
