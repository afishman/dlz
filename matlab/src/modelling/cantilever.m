function beam = cantilever(E, L, b, d, m, em, ei, V, nu)
beam = struct;
beam.E = E; % Flexural Stiffness
beam.L = L; % Beam length
beam.b = b; % Beam width
beam.d = d; % Beam depth
beam.m = m; % Mass of endpoint
beam.em = em; % Permittivity of membrane
beam.ei = ei; % Permittivity of interface
beam.V = V; % Voltage
beam.nu = nu; % effective visociosty

beam.e0 = 8.85e-12; % Permittivity if free space
beam.g = 9.81; % Acceleratio
beam.I = (1/12)*b*d^3; % Moment of inertia
beam.EI = beam.E*beam.I; % Flexural Stiffness

