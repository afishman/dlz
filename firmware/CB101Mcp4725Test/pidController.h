class PidController 
{ 
    public:
        float Hd;            // Desired Height
        float HPrev;         // Previous height (to calculate height velocity)
        
        float Ki;            // Integral Gain
        float Kp;            // Proportional Gain
        float Kd;            // Derivative Gain

        float VMax;          // Maximum Voltage
        float Vs;           // Voltage Level (constant term added to controller output)
                      
        float ErrorIntegral; // Error Integral
        float ERate;         // Error Rate
        float E;             // Error


        float propotionalVoltage();
        float integralVoltage();        
        float derivativeVoltage();
        PidController(float h, float hd, float kp, float ki, float kd, float vMax, float vs);
        
        // Height, time and change in time since last timestep
        float update(float h, float t, float dt);
};
  
