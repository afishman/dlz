function photo(n)
%% Take photo by triggering with the return key

% Number of snaps
if ~exist('n', 'var')
    n = 1;
end

% Attach to camera
vid = camera;
vid.FramesPerTrigger = 1;

% Preview
preview(vid);
pause(0.5); % HACK: Sometimes preview doesn't show without a pause

% Setup triggering
triggerconfig(vid, 'manual');
vid.TriggerRepeat = n-1;
start(vid);

% Take it away
for i=1:n
    filepath = [datestr(now,'dd-mmm-yy--HH-MM-SS'), '.jpg'];
    
    %Get filepath from user
    prompt = {'Save as'};
    dlgtitle = 'Snapshot';
    dims = [1 35];
    definput = {filepath};
    answer = inputdlg(prompt,dlgtitle,dims,definput);
    filepath = answer{1};

    % Acquire snapshot
    trigger(vid);
    img = getdata(vid);
    
    % Write to disk
    imwrite(img, filepath);
    
    disp(['saving as ', filepath]);
end

stop(vid)
stoppreview(vid)
closepreview
