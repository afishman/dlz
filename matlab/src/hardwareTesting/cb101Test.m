function data = cb101Test(port, duration)
if ~exist('port', 'var')
    port = '/dev/tty.usbmodem1411';
end

arduino = serial(port, 'BaudRate',115200);
fopen(arduino);
onCleanup(@()fclose(arduino));

%% Perform Trial
tStart = tic;
t = 0;
data = [];
while t<duration
    t = toc(tStart);
    
    % Read Line and parse
    str = fgetl(arduino);
    nums = str2double(strsplit(str,','));
    
    % Create table line
     %<t>, <height>, <voltage monitor>, <current monitor>, <set voltage>, <p>, <i>, <d>
    dataline =  array2table([nums t], 'VariableName', {
        'time', ...
        'voltage_monitor', ...
        'current_monitor', ...
        'control_voltage', ...
        'tMatlab' ...
    });
    
    data = [data; dataline];
end