#include <Wire.h>
#include <Adafruit_MCP4725.h>

Adafruit_MCP4725 DAC;

// This is used to test the high voltage PSU

// From CB-101 Datasheet
// https://www.xppower.com/portals/0/pdfs/SF_CB_Series.pdf
float HIGH_VOLTAGE_PSU_OUTPUT_VOLTS = 10000;
float HIGH_VOLTAGE_PSU_OUTPUT_MICROAMPS =  100;

int VOLTAGE_MONITOR_PIN = A9;
int CURRENT_MONITOR_PIN = A10;

void setup(void) {
  Serial.begin(115200);

  // For Adafruit MCP4725A1 the address is 0x62 (default) or 0x63 (ADDR pin tied to VCC)
  DAC.begin(0x62);
}


void loop() 
{  
      // 1Hz Test Signal 
      float t = ((float)millis())/1000;
      float f = 1;
      float voltage = HIGH_VOLTAGE_PSU_OUTPUT_VOLTS*0.5*(1+sin(2*PI*f*t));
      setHv(voltage);

      // Print
      String message = String(readVoltageMonitor()) + "," + String(readCurrentMonitor());
      Serial.println(message);
}

float readVoltageMonitor()
{
    return (HIGH_VOLTAGE_PSU_OUTPUT_VOLTS/1023) * ((float)analogRead(VOLTAGE_MONITOR_PIN));
}

float readCurrentMonitor()
{
    return (HIGH_VOLTAGE_PSU_OUTPUT_MICROAMPS/1023) * ((float)analogRead(CURRENT_MONITOR_PIN));
}

// Set the high voltage. Saturates at 0 and HIGH_VOLTAGE_PSU_OUTPUT
void setHv(float voltage)
{
    // Ensure voltage is in an allowable range
    if(voltage > HIGH_VOLTAGE_PSU_OUTPUT_VOLTS)
    {
       voltage = HIGH_VOLTAGE_PSU_OUTPUT_VOLTS;
    }
    else if(voltage < 0)
    {
       voltage = 0;
    }

    // Set Hv
    uint16_t dacLevel = (uint16_t) 4095*voltage/HIGH_VOLTAGE_PSU_OUTPUT_VOLTS;
    DAC.setVoltage(dacLevel, false);
}
