close all;
root = '/Volumes/Oarfish/dlz/cantilever';

files = listFiles(root, '*.mat');

figure; hold on;
labels = {};

for i=1:length(files)
    filepath = files{i};
    if strfind(filepath, '._')
        continue
    end
    
    data = load(filepath);
    
    voltage = data.metaData.voltage;
    plot(data.data.time, 1000*data.data.height)
    labels{end+1} = num2str(voltage);
end

grid on
ylim([-5, 0]);

legend(labels)