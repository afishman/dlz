function filenames = listFiles(dirName, pattern)
    % List Files given a glob patten
    % e.g. '*.mat'
       
    % Add trailing slash
    if ~strcmp(dirName(end), '/') && ~strcmp(dirName(end), '\')
        dirName = strcat([dirName, '/']);
    end
        
    fileInfos = dir([dirName, '/', pattern]);
    
    filenames = cell(length(fileInfos),1);
    
    for i=1:length(fileInfos)
        filenames{i} = strcat([dirName, fileInfos(i).name]);
    end
    
end