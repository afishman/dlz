function h = sineyTime(t)
    osc1Center = -20;
    osc1Center = -20;
    osc1Amp = 7;
    osc1Freq = 1/10;
    numOscillation1 = 3;
   
    osc2Center = -20;
    osc2Amp = 5;
    osc2Freq = 1/5;
    numOscillation2 = 3;
    
    osc3Center = -20;
    osc3Amp = 2;
    osc3Freq = 1/2;
    numOscillation3 = 3;
    
    interOscillationPauseTime = 5; 
    
    rampcaseHeights = [-34.0, -34.0, osc1Center, osc1Center];
    rampcaseTimes = [0, 10, 15, 20];
    
    sine1End = rampcaseTimes(end) + numOscillation1*(1/osc1Freq);
    interOscillationTimeEnd = sine1End+interOscillationPauseTime;
    sine2End = interOscillationTimeEnd + numOscillation2*(1/osc2Freq);
    pause2End = sine2End+interOscillationPauseTime;
    osc3End = pause2End + numOscillation3*(1/osc3Freq);
    
    if t<rampcaseTimes(end)
        h = rampcase(t, rampcaseHeights, rampcaseTimes);
        
    elseif t<sine1End
        phase = rampcaseTimes(end);
        h = osc1Amp*sin(2*pi*osc1Freq*(t-phase)) + osc1Center;
        
    elseif t<interOscillationTimeEnd
        h = osc1Center;
        
    elseif t<sine2End
        phase = interOscillationTimeEnd;
        h = osc2Amp*sin(2*pi*osc2Freq*(t-phase)) + osc2Center;
        
    elseif t<pause2End
        h = osc2Center;
        
    elseif t<osc3End
        phase = interOscillationTimeEnd;
        h = osc3Amp*sin(2*pi*osc3Freq*(t-phase)) + osc3Center;
    
    else
        h = osc2Center;
        
    end

    

end


function hd = rampcase(t, heights, times)

index = find(t<times, 1, 'first');

if isempty(index)
    hd = heights(end);
    return
end

h2 = heights(index);
t2 = times(index);

if index==1
    h1 = h2;
    t2 = t2;

else
    h1 = heights(index-1);
    t1 = times(index-1);
end

m = (h2-h1)/(t2-t1);
hd = h1+ m*(t-t1);


end