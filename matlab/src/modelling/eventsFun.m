function [position,isterminal,direction] = eventsFun(t,y)
position = y(1)- 1e-3; % The value that we want to be zero
isterminal = 1;  % Halt integration 
direction = 0;   % The zero can be approached from either direction