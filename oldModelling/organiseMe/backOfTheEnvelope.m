clear all

% https://www.makeitfrom.com/material-properties/SAE-AISI-1095-SUP4-1.1274-C100S-G10950-Carbon-Steel
thickness = 50e-6; % 50->200um;
width = 20e-3;
rho = 7.8* 1000^3;

E = 190e9;
I = (1/12)*width*thickness^3;
G = 72e9;
L = 0.1;
k = 5/6;



A = thickness*width;

coefficient = E*I/(k*A*G);