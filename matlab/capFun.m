function c = capFun(x, beam, h)
c = [];
for i=1:length(x)
    c(i) = beam.e0*beam.m*beam.b / ((0.5e-3) + deflection(beam, x(i), h));
end
