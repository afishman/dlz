function s = cameraSettings
%% Connect to a Point Grey Grasshopper camera

s = struct;
s.bayer = 'F7_BayerGB8_2048x2048_Mode0';
s.FrameRate = 30;
