function plotHistograms(img, settings)
%% settings
if ~exist('settings', 'var')
    settings = defaultDetectSettings;
end

% Subplot
nrows = 2;
ncols = 3;
count = 0;



%% Convert to hsv
imgDouble = double(img)/255;
hsv = rgb2hsv(imgDouble);
h = hsv(:,:,1);
s = hsv(:,:,2);
v = hsv(:,:,3);


%% Hue
count=count+1; subplot(nrows, ncols, count); hold on;

title('Hue')
hist(h(:), 255)
vline(settings.lid_hue(1))
vline(settings.lid_hue(2))

vline(settings.beam_hue(1))
vline(settings.beam_hue(2))


%% Saturation
count=count+1; subplot(nrows, ncols, count); hold on;
title('Sat')
hist(s(:), 255)
vline(settings.lid_sat(1))
vline(settings.lid_sat(2))

vline(settings.beam_sat(1))
vline(settings.beam_sat(2))

%% Value Histogram 
count=count+1; subplot(nrows, ncols, count); hold on;
title('Value')
hist(v(:), 255)

vline(settings.lid_value(1))
vline(settings.lid_value(2))

vline(settings.beam_value(1))
vline(settings.beam_value(2))

%% Hue Plane
count=count+1; subplot(nrows, ncols, count); hold on;
title('Hue Plane')
imagesc(h)

%% Saturation Plane
count=count+1; subplot(nrows, ncols, count); hold on;
title('Saturation Plane')
imagesc(s)

%% Value Plane
count=count+1; subplot(nrows, ncols, count); hold on;
title('Value Plane')
imagesc(v)
