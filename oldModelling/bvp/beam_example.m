% THanks: https://uk.mathworks.com/matlabcentral/answers/162207-using-bvp4c-to-solve-a-fourth-order-nonlinear-differential-equation-with-2-boundary-conditions

function mat4bvp
options = bvpset('RelTol', 1e-6, 'AbsTol', 1e-6, 'NMax', 10000);
solinit = bvpinit(linspace(0,5.8e-3,10),[1 0 0 0]);
sol = bvp4c(@mat4ode,@mat4bc,solinit,options);
x = linspace(0,5.8e-3);
y = deval(sol,x);
plot(x,y(1,:));

function dxdy = mat4ode(~,y)
E=169e9; % YMod
w=230e-9; % Width of beam
t=220e-9; % Thickness of beam
I=(t*(w^.3))/12; % Moment of intertia
V=10; % Voltage
e0=8.854187817e-12; % VPerm
const=((0.5*(V.^2)*e0*t)/(E*I)); %All constant values
s0=100e-9; % Initial Separation
dxdy=[y(2)
      y(3)
      y(4)
      const/((s0-2*y(1)).^2)]; %4th order equation

function res = mat4bc(ya,yb)
res=[ya(1) % a=0 at x=0
  ya(2) % da/dx=0 at x=0
  yb(1) % a=0 at x=L
  yb(2)]; % da/dx=0 at x=L