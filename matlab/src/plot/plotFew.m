close all

labels = {};
load('dlz_trial_11-13-03_3400V.mat');
labels{end+1} = '3400';
plotData(data)

hold on

load('dlz_trial_11-28-11_3450V.mat');
labels{end+1} = '3450';
plotData(data)


load('dlz_trial_11-19-17_3500V.mat');
labels{end+1} = '3500';
plotData(data)

load('dlz_trial_11-31-43_4000V.mat');
labels{end+1} = '4000';
plotData(data)

load('dlz_trial_11-34-42_5000V.mat');
labels{end+1} = '5000';
plotData(data)

load('dlz_trial_11-38-32_5500V.mat');
labels{end+1} = '5500';
plotData(data)

load('dlz_trial_11-39-17_6000V.mat');
labels{end+1} = '6000';
plotData(data)


legend(labels);
