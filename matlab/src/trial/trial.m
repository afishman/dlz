function data = trial(session, duration, voltageFun, varargin)
%% Generic Config
fileNameRoot = 'dlz_trial_';

%% Turn off HV rig on completion
% TODO: Make sure number of outputs is correct
cleanupObj = onCleanup(@() outputSingleScan(session.highVoltageControlSession,[0 0 0]));

%% Parse Inputs
p = inputParser;
addOptional(p, 'video', false);
addOptional(p, 'overfilm', 0);
addOptional(p, 'dirName', './');
addOptional(p, 'metadata', struct);
addOptional(p, 'filename', fileNameRoot);
parse(p, varargin{:});

metaData = p.Results.metadata;
dirName = p.Results.dirName;
video = p.Results.video;
overfilm = p.Results.overfilm;
filename = p.Results.filename;

%% Init Camera
if video
    vid = camera;
    vid.FramesPerTrigger = Inf;

    % Heads-up display
    preview(vid);
    pause(0.5);

    start(vid);
end

%% Perform Trial
% Turn into a cell array if one function handle is passed
if isa(voltageFun, 'function_handle')
    voltageFun = {voltageFun};
end

% Determine trial type
if isa(voltageFun, 'PidController')
    if length(voltage_fun) ~= 1
        error('Pid Controller only works across two channels')
    end
    [time, rawData, pidData] = pidTrial(session, voltageFun, duration);
    
elseif isa(voltageFun, 'cell')
    nargs = max(cellfun(@(x)nargin(x), voltageFun));
    
    % Use fast control frequency if voltages can be precomputed
    if nargs == 1
        [time, rawData] = queuedTrial(session, voltageFun, duration);
    
    % Slower feedback trial
    elseif nargs <=4
        if length(voltageFun) ~= 1
            error('feedbackTrial only works across two channels currently')
        end
        [time, rawData] = feedbackTrial(session, voltageFun{1}, duration);       
        
    else
        error('can only run trails with voltage functions up tp 4 args');
    end        

else 
    error('Could not match trial type for voltage function')
end

% Output Data
data = processRawData(session, time, rawData);

% Concatenate raw data into a table
% TODO: Include output voltages
rawDataTable = array2table([time, rawData],  'VariableNames', [{'time'}, session.inputLabels]);

%% Save output data
if ~exist('pidData', 'var')
    pidData = [];
else
   data = [data, pidData]; 
end

% Ensure output directory exists
if ~exist(dirName, 'dir')
    mkdir(dirName)
end

%TODO: Check that the file doesn't already exist
dateString = datestr(now,'dd-mmm-yy--HH-MM-SS');
if strcmp(filename, fileNameRoot)    
    saveString = [fullfile(dirName, fileNameRoot), dateString];
else
    saveString = filename;
end

disp(['saving as ' saveString]);

% Get sha of current commit
try
    [~, sha] = system('git rev-parse HEAD'); 
    sha = sha(1:end-1);
catch
    sha = '';
end

% TODO: include metaData?
save(saveString, 'rawDataTable', 'data', 'metaData', 'voltageFun', 'duration', '', 'dateString', 'sha');

% Write to disk
if video
    fprintf('Trial finished\n')
    if round(overfilm)
        % Film a bit extra
        fprintf('filming %is\n', round(overfilm))
    end
    pause(overfilm);
    stop(vid);
    closepreview;
    render(vid, [saveString, '.avi'])
end