close all

%% Load Data
load('/Users/aaron/Documents/DLZ/cantilever/run2/beam.mat')

%% Fit Flexural Stiffness
idx = 6;
beamResponse.weight = beamResponse.weight;
fit = polyfit(beamResponse.weight(idx:end), beamResponse.deflection(idx:end),1); % x = x data, y = y data, 1 = order of the polynomial.

% Back of the envelope flexural stiffness
rho = 7.8 * 1000^3; % Density kg/m3
b = 12.7e-3; % Width (m)
d = 0.5e-3; % Thicknss (m)
l = 125e-3; %Length (m)
I = (1/12)*b*(d^3); %Moment of Intertia
E = 190e9; % Youngs modulus (Pa)
m = 17.7e-3; % Mass of end point (g)
em = 2.75; % TODO: Calculate these
ei = 4.62; % TODO: Calculate these
V = 0;
nu = 0;

% Predicted Flexural Stiffness
EIpredicted = E*I;

% Experimental Flexural Stiffness
% (eqn for end point deflection rearranged for EI)
EIexperiment = g*(l^3) / (3*fit(1));

fprintf('Predicted: '); disp(EIpredicted);
fprintf('Actual: '); disp(EIexperiment);


%% Make Cantilever
beam = cantilever(E, l, b, d, m, em, ei, V, nu); 

%% Plot
hold on
plot(1000*beamResponse.weight, 1000*polyval(fit,beamResponse.weight))
scatter(1000*beamResponse.weight, 1000*beamResponse.deflection)
xlabel('Load (g)')
ylabel('Deflection (mm)')
legend('Best Fit', 'Experimental Data')
grid on
