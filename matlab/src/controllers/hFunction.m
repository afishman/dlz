function hd = hFunction(t)
    
% first hd with sd
    hd1 = -20;
    td1 = 10; % time driving
        sd1 = hd1*td1;
    
    th1 = 5; % time holding
    tt1 = td1+th1; % time total

% second hd with sd
    hd2 = -15;
    td2 = 5;
        sd2 = hd2*td2;
    
    th2 = 5;
    tt2 = td2 +th2;
    
% process
    % driving 1
    if t <= td1
        disp('1')
        hd = -28 - sd1*(t-0);
    % holding 1
    elseif t > td1 && t <= tt1 %td1+th1
        disp('2')
        hd = hd1;
    % driving 2
    elseif t > tt1 && t <= tt1+td2
                disp('3')
        hd = hd1 - sd2*(t-tt1);
    % holding 2
    else % t < tt1+tt2
                disp('4')
        hd = hd1+hd2;
    end

% sd = hd*t;
