#include "cb101.h"

// Main Controls
float VOLTAGE = 9000;

// Let/Right Pushbuttons
int LEFT_BUTTON = 10;    
int RIGHT_BUTTON = 11;

// For Voltage Feedback
int LED_PIN = 2;

// 1st PSU Pinout
int DAC1_ADDRESS = 0x62;
int VM1_PIN = A9;
int CM1_PIN = A10;

// 2nd PSU Pinout
int DAC2_ADDRESS = 0x63;
int CM2_PIN = A3;
int VM2_PIN = A4;

int LOOP_DELAY_MS = 30;

// CB101 Objects
Cb101* PSU1;
Cb101* PSU2;

void setup() {
    Serial.begin(115200);

    // Initialise CB101
    PSU1 = new Cb101(DAC1_ADDRESS, VM1_PIN, CM1_PIN);
    PSU2 = new Cb101(DAC2_ADDRESS, VM2_PIN, CM2_PIN);

    // Left/Right Buttons
    pinMode(LEFT_BUTTON, INPUT);  
    pinMode(RIGHT_BUTTON, INPUT);

    PSU1->setHv(0);
    PSU2->setHv(0);
    
}

void loop() 
{    
    // voltage in volts
    //float t = ((float) millis())/1000;
    //float v = VOLTAGE*0.5*(sin(t)+1);

    int left_button_val = digitalRead(LEFT_BUTTON);
    int right_button_val = digitalRead(RIGHT_BUTTON);

    if(left_button_val && right_button_val)
    {
        PSU1->setHv(0);
        PSU2->setHv(0);
        setLed(0.25);
    }
    else if(left_button_val && !right_button_val)
    {
        PSU1->setHv(0);
        PSU2->setHv(VOLTAGE);
        setLed(1);
    }
    else if(!left_button_val && right_button_val)
    {
        PSU1->setHv(VOLTAGE);
        PSU2->setHv(0);
        setLed(0);
    }
    
    // Print
    printCurrents();

    // Sleep
    delay(LOOP_DELAY_MS);
}

// Logging
void printVoltages()
{
    String message = "";
    message += String(PSU1->readVoltageMonitor()) + ",";
    message += String(PSU2->readVoltageMonitor()) + ",";
    Serial.println(message);
}

void printCurrents()
{
    String message = "";
    message += String(PSU1->readCurrentMonitor()) + ",";
    message += String(PSU2->readCurrentMonitor()) + ",";
    Serial.println(message);
}


// intensity should be between 0 (off) and 1 (full brightness)
void setLed(float intensity)
{    
    analogWrite(LED_PIN, 255*constrain(intensity, 0, 1));  
}
