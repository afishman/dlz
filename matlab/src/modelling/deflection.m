function y = deflection(beam, x, h)
if exist('h', 'var')
    P = h * 3*beam.EI / beam.L^3;
else
    P = beam.m * 9.81;
end

% Steady state deflection of a cantilever beam: https://en.wikipedia.org/wiki/Euler%E2%80%93Bernoulli_beam_theory#Cantilever_beams
y = (P*x.^2) .* (3.*beam.L-x) ./ (6*beam.EI);