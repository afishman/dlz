function txt = plotParetoClickHandler(trash, event, param1, param2, benchmarkData)
    param1Equal = cellfun(@(x)x.(param1)==event.Position(1), benchmarkData);
    param2Equal = cellfun(@(x)x.(param2)==event.Position(2), benchmarkData);

    index = find(param1Equal+param2Equal, 1, 'first');
    
    b = benchmarkData(index);
    b = b{1};

    txt = sprintf('%s\nP: %i\nI: %i\nD: %i\n', b.filename, b.p, b.i, b.d);
end