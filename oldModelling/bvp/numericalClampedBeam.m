% THanks: https://uk.mathworks.com/matlabcentral/answers/162207-using-bvp4c-to-solve-a-fourth-order-nonlinear-differential-equation-with-2-boundary-conditions
function [x,y] = numericalClampedBeam(L, EI, w, meshSize)
%% Example
% n_cb(1, 1,@(x) 1)
% EI = 1;
% w = @(x) 1;
%%

if ~exist('meshSize') 
    meshSize = 10;
end

% Initial guess domain
x = linspace(0,L, meshSize);

% Solve Euler-Bernoulli BVP
options = bvpset('RelTol', 1e-6, 'AbsTol', 1e-6, 'NMax', 10000);
solinit = bvpinit(x, [1 10 10 1]);
sol = bvp4c(@(x,y)mat4ode(x,y,w,EI), @mat4bc, solinit, options);

% Return Solution
x = linspace(0,L, 10*meshSize);
y = deval(sol,x);
y = y(1,:);

% Euler-Bernoulli beam equation for a distributed load
function dydx = mat4ode(x,y,w,EI) 

if nargin(w) == 1
    beamLoad = w(x);
    
elseif nargin(w) ==2
    beamLoad = w(x, y(1));
    
else
    error('w must be a function of form w(x), w(x,y)')
end
   
fprintf('******\nx: %.2f\ny: %.2f\nw: %.2f\n', x,y,beamLoad);

%4th order equation
dydx=[y(2);
      y(3);
      y(4);
      beamLoad/EI
]; 

% Clamped B.C's
function res = mat4bc(ya,yb)
res=[ya(1) % a=0 at x=0
  ya(2) % da/dx=0 at x=0
  yb(1) % a=0 at x=L
  yb(2)]; % da/dx=0 at x=L