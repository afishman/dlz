function root = findRoot(beam)
% Finds the lowest root it can find with brute force

rootSearchPts = 1000;

vals = [];
x = linspace(0, naturalDeflection(beam)+1e-3, rootSearchPts);
for j=x
    vals(end+1) = rootFun(j, beam);
end

crossings = sort(find(diff(sign(vals))~=0));

if isempty(crossings)
    root = 0;
else
    root = x(max(crossings));
end