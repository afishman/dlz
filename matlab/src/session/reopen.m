function newSession = reopen(session)
    %% Releases session resource and reinstates a new one
    releaseDlzSession(session);
    newSession = dlzSession;