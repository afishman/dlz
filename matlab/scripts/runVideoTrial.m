voltage = 4000;

%% Hat Function
offTime = 2;
onTime = 5;

duration = onTime+offTime;
voltageFuns = {@(t) hatFunction(t, voltage, onTime, offTime)};

%% Plot
close all
plotVoltageFun(voltageFuns, duration)

%% Run
for i=1:1
    data = trial(session, duration, voltageFuns, 'video', true, 'overfilm', 3);
end

close all
plotData(data)