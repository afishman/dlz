function [time] = riseTime(pidController, data, tol)
    %% How long it takes to get within a certain tolerance of the target
    %height

    % Default tolerance mm
    if ~exist('tol')
        tol = 0.5;
    end    
     
    %Height
    hd = pidController.hd;
    index = find(data.height>(hd-tol), 1, 'first');
    
    % If it never makes it 
    if isempty(index)
        time = data.time(end);
        return
    end
    
    time = data.time(index);
    
end

