function [height, props] = detect(img, settings, debug)
props = struct;
props.img = img;

if ~exist('detectSettings', 'var') || isempty(settings)
    settings = defaultDetectSettings;
end

if ~exist('debug', 'var')
    debug = false;
end

% Convert to hsv
imgDouble = double(img)/255;
hsv = rgb2hsv(imgDouble);
h = hsv(:,:,1);
s = hsv(:,:,2);
v = hsv(:,:,3);

%% Lid
% Threshold
lid_mask = detectMask(hsv, settings.lid_hue, settings.lid_sat, settings.beam_value);
lid_mask3 = repmat(lid_mask, [1,1,3]);

% Detect boundaries
[lidContours,L] = bwboundaries(lid_mask, 'holes');
if length(lidContours) < 1
    error('Could not find lid')
end

% Sort by Area
lidContours = sortByKey(lidContours, @(x) polyarea(x(:,2), x(:,1)), 'descend');

% The largest contour is the outline of the image,
% Assume the cantilever lid is second biggest part
lid = lidContours{1};
props.lid = lid;
props.lidContours = lidContours;

%% Beam Endpoint
% Threshold
beam_mask = detectMask(hsv, settings.beam_hue, settings.beam_sat, settings.beam_value);
beam_mask3 = repmat(beam_mask, [1,1,3]);

% Detect boundaries
[beamContours, L] = bwboundaries(beam_mask, 'holes');
if length(beamContours) < 1
    if debug
        beamContours = {[]};
    else
        error('Could not find beam')
    end    
end

% Sort by Area
beamContours = sortByKey(beamContours, @(x) polyarea(x(:,2), x(:,1)), 'descend');

% Assume the cantilever lid is largest contour
beam = beamContours{1};
props.beamContours = beamContours;
props.beam = beam;

%% Height
[~, I] = max(lid(:,1)+lid(:,2));
baseHeight = lid(I, 1);
props.baseHeight = baseHeight;

[~, I] = min(beam(:,1)+beam(:,2));
beamHeight = beam(I,1);
beamX = beam(I,2);

props.beamHeight = beamHeight;
props.beamX = beamX;

mm_per_pixel = settings.lid_length_mm/(max(lid(:,2)) - min(lid(:,2)));
height = mm_per_pixel*(beamHeight- baseHeight);

%% Return
if ~debug
    return
end

%% Detection Plot
figure; hold on
plotDetect(props);

%% HSV Planes & Histograms
figure
plotHistograms(img, settings);

%% Mask
figure ; hold on
title('Lid mask')
imagesc(lid_mask3)

figure ; hold on
title('Beam mask')
imagesc(beam_mask3)


