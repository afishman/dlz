function hDdot = nullclineFun(h, hDot, beam)
    dydt = odeFun(0, [h, hDot], beam);
    hDdot = dydt(2);
end