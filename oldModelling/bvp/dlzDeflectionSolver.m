em = 2.7; % Silicon oil
ei = 4.62; % PVC tape
e0 = 8.85418782e-12; % Free space permittivity

b = 0.025; % Width of actuator
L = 0.1; % Length of actuator

ti = 0.001; % Thickness of insulator
tm = 0.001; % Thickness of oil?

V = 10000;

w = @(x,y) -0.5*em*e0*b*V*V/( ti*em/ei - y ).^2;
%w = @(x,y) 0;

EI = 1;

close all
labels = {};
for i=linspace(1000, 10000, 10)
    V = i;
    w = @(x,y) 0.5*em*e0*b*V*V/( ti*em/ei + y ).^2;
    
    labels{end+1} = sprintf('%iV', i);
    [x,y] = numericalClampedBeam(L, EI, w);
    plot(x,y);
    hold on
end
legend(labels)
xlabel('Length(m)')
ylabel('Deflection(m)')
grid on