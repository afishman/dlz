function voltage=hatFunction(t, voltage, onTime, offTime)
    %% Original Hat Function that Tim/Majid used
    if t<offTime || t>(onTime+offTime)
        voltage=0;
    end
end