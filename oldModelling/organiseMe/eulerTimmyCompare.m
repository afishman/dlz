clear all
close all

% https://www.makeitfrom.com/material-properties/SAE-AISI-1095-SUP4-1.1274-C100S-G10950-Carbon-Steel
thickness = 0.05e-3; % 50->200um;
width = 25e-3;

E = 190e9;
I = (1/12)*width*thickness^3;
G = 72e9;
L = 100e-3;
k = 5/6;
A = thickness*width;
g = 9.8;

m = 9e-3;
P = m*g;

coefficient = E*I/(k*A*G);

x = linspace(0,L,100);

wE = (P*(x.^2).*(3*L-x))/(6*E*I);
wT = P*x./(k*A*G) - (P.*(L-x))./(2.*E.*I).*((L.^2) - ((L-x).^2)./3) + (P*L^3)/(3*E*I);

fprintf('Deflection mm (Euler): %.0f\n', 1000*P*L^3/(3*E*I))
fprintf('Deflection mm (Timos): %.0f\n', 1000*wT(end))

plot(x, wE)
hold on
plot(x, wT)
legend('Euler', 'Timoschenko')
xlabel('Beam Axis x (m)')
ylabel('Deflection (m)')

figure
plot(x, wE - wT)