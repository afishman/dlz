function outputLine = outputDataLine(session, voltage)
    % Split over two channels
    channelVoltage = scaleOutputVoltage(session, voltage);
    
    % Queue voltage
    %TODO: Ensure that column1 and column2 corresponds to
    outputLine = zeros(1, length(session.outputLabels));
    outputLine(:, 1) = -channelVoltage;
    outputLine(:, 2) = channelVoltage;
end