function plotPareto(dirName, param1, param2)
    if ~exist('param1')
        param1 = 'riseTime';
    end
    
    if ~exist('param2')
        param2 = 'overshoot';
    end

    %% Do a pareto plot
    filenames = listFiles(dirName, '*.mat');
    benchmarkData = cell(0,1);
    
    params1 = [];
    params2 = [];
    
    % Loop over each file and collect benchmarks
    for i=1:length(filenames)
        filename = filenames{i};
        load(filenames{i})
        
        % Skip over non-pid controllers atm
        if ~isa(voltageFun, 'PidController')
            disp({'skipping', filename})
            continue
        end
        
        % Skip over variable set point for now..
        if isa(voltageFun.hd, 'function_handle')
            disp({'skipping', filename})
            continue
        end
        
        % Add to benchmarks
        b = benchmarks(voltageFun, data);
        b.filename = filename;
        benchmarkData{end+1} = b; 
        params1(end+1) = b.(param1);
        params2(end+1) = b.(param2);
    end
    
    % Pareto Front
    if length(filenames)>2
        hold on
        hullIndices = convhull(params1, params2);

        [C,minIndex1] = min(params1);
        [C,minIndex2] = min(params2);

        i1 = find(hullIndices==minIndex1, 1, 'first');
        i2 = find(hullIndices==minIndex2, 1, 'first');

        paretoIndices = hullIndices(i1:i2);

        plot(params1(paretoIndices), params2(paretoIndices), 'r')
    end 
    
    % Scatter Plot
    scatter(params1, params2);
    xlabel(param1)
    ylabel(param2)

    % Clickity Click
    datacursormode on
    dcm = datacursormode(gcf);
    set(dcm,'UpdateFcn',@(t,e)plotParetoClickHandler(t,e, param1, param2, benchmarkData))
    
    grid on    
end
