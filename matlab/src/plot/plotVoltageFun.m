function plotVoltageFun(voltageFuns, duration)
    if isa(voltageFuns,'function_handle')
        voltageFuns = {voltageFuns};
    end
    hold on;
    labels = {};
    for i=1:length(voltageFuns)
        t = linspace(0, duration, 100);
        v = arrayfun(voltageFuns{i}, t);
        plot(t,v);
        labels{end+1} = sprintf('Channel %i', i);
    end
    legend(labels);
    xlabel('Time (s)')
    ylabel('Voltage (V)')
    grid on

end