close all

%% Load Data
experimentsPath = '/Users/aaron/Documents/DLZ/cantilever/run2/';
load([experimentsPath, 'beam.mat']);
%ei = 0.8788;
%beam.ei = ei;

experimentFilepaths = listFiles(experimentsPath, '*V.mat');

experiments = {};
for i = 1:length(experimentFilepaths)
    filepath = experimentFilepaths{i};
    
    experiment = load(filepath);
    experiments{end+1} = experiment;
end
experiments = horzcat(experiments{:});

%% End height
tInf = 9.9;
hInf = arrayfun(@(x) x.data.height(...
    find(x.data.time >= tInf, 1)...
), experiments);
voltages = arrayfun(@(x) x.metaData.voltage, experiments);

%% Predicted
close all; beam.ei = 1.5;

hInfPred = [];
for i=1:length(experiments)
    experiment = experiments(i);
    beam.V = experiment.metaData.voltage;
    
    root = findRoot(beam);
    hInfPred(end+1) = findRoot(beam);
end

figure; hold on; grid on;
plot(voltages, hInf);
plot(voltages, -hInfPred);
xlabel('voltage')
ylabel('h')
legend('experiment', 'sim')

%% Plot


load([experimentsPath, 'beam.mat']);
idx = 4; % Beam @ 1500V
beam.V = voltages(idx);
hPred = hInf(idx);
es = linspace(0, 1, 100);

roots = [];

count = 0;
for i=1:length(es)
    count = count+1;
    fprintf('%i of %i\n', count, length(es));
    beam.ei = es(i);
    
    roots(end+1) = findRoot(beam);
end

eBruteForces = es(max(find(diff(sign(roots+hPred))~=0)));

%
figure
plot(es, roots+hPred)
xlabel('em')
ylabel('height')

%% More plots
% Hinf plot
figure; hold on
plot(voltages,hInf);
xlabel('Voltage (V)')
ylabel('hInf')

% Time series plot
figure; hold on; grid on
labels = {};
for i=1:length(experiments)
    experiment = experiments(i);
    
    plot(experiment.data.time, experiment.data.height);
    
    labels{end+1} = sprintf('%.0fV', experiment.metaData.voltage);
    legend(labels);
end

% Blah Plot