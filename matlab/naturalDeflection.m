function h = naturalDeflection(beam)

h = g*beam.m*beam.L^3/(3*beam.EI);