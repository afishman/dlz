function session = demo1Session(port)
if ~exist('port', 'var')
    port = '/dev/tty.usbmodem1411';
end

session = struct();

session.serial = serial(port, 'BaudRate',115200);
fopen(session.serial);

end