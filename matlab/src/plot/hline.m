function hline(y,fmt)
limits = xlim(gca);

if ~exist('fmt')
   fmt = 'k:';
end

hold on
plot(limits, [y,y], fmt);
