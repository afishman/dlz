function [voltage, int_new] = piController(t, h, v, dt, int, Ki, Kp, hd, vMax)
    % K: integral gain
    % hd: desired height
    E = (hd-h);
    int_new = int + Ki*E*dt;
    voltage = E*Kp + int_new;
    
    if voltage > vMax
        voltage = vMax;
    end
    
    % For debugging
    disp({'int', int_new})
    disp({'voltage', voltage})
end