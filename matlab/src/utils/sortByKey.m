function sorted = sortByKey(arr, key, direction)
    if ~exist('direction', 'var')
        direction = 'ascend';
    end

    % Evaluate key
    if iscell(arr)
        vals = cellfun(key, arr);
    else
        vals = arrayfun(key, arr);
    end
    
     % Sort
    [~,I] = sort(vals, direction);
    sorted = arr(I);
end