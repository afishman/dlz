function beam = defaultBeam

% Beam properties
m = 10e-3; % Point mass (kg)
E = 190e9;    %   E young's modulus /N/m (SAE-AISI 1095 carbon steel)
L = 100e-3; % Cantilever Length (m)
b = 20e-3; % Canbtilever width (m)
d = 0.1e-3; % Thickness of beam (m)
em = 2.75; % silicone oil http://www.clearcoproducts.com/pdf/pure-silicone/Dielectric_Properties_Pure_Silicone_Fluids.pdf
ei = 4.62; % Permittivity of insulator
nu = 0.5; % TBD
V = 10e3; % Voltage (V)

beam = cantilever(E, L, b, d, m, em, ei, V, nu);
