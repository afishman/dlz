close all

experimentsPath = '/Users/aaron/Documents/DLZ/cantilever/run3/';
experimentFilepaths = listFiles(experimentsPath, '*V.mat');


%% HACK: Think I miscalibrated the laser, rezero experimental data by matching 0v case
v0 = load(experimentFilepaths{1});
%beam.ei = 0.8;
beam.nu = 15;
beam.em = 65;

sol = beamSim(beam, [0, v0.data.time(end)]);


offset = -naturalDeflection(beam) - mean(v0.data.height);

hold on
plot(v0.data.time, v0.data.height)
plot(sol.x, -(sol.y(1,:) + offset))



%% Integrate model for each experiment

% Plot
labels = {};
sols = [];
hold on; grid on
for i=10:length(experimentFilepaths)
    fprintf('%i of %i\n',i, length(experimentFilepaths));
    filepath = experimentFilepaths{i};
    
    if contains(filepath, 'beam.mat')
        continue
    end
    
    if i>3
        break
    end
    
    % Load experiment
    experiment = load(experimentFilepaths{i});
    experiment.data.height(experiment.data.height>0) = 0;
    beam.V = experiment.metaData.voltage;
    
    % Integrate model
    sol = beamSim(beam, [0, experiment.data.time(end)]);
    
    % Plot
    
    plot(experiment.data.time, experiment.data.height);
    plot(sol.x, -(sol.y(1,:) + offset));
    
    labels{end+1} = sprintf('%.0f V Exp', beam.V);
    labels{end+1} = sprintf('%.0f V Model', beam.V);
    
end

legend(labels)
