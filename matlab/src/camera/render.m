function render(vid, filepath)
%% Render camera footage stored in memory to disk

% Prepare output file
diskLogger = VideoWriter(filepath, 'Motion JPEG AVI');
src = getselectedsource(vid);
diskLogger.FrameRate = src.FrameRate;
open(diskLogger);

% Write data
data = getdata(vid, vid.FramesAvailable);
numFrames = size(data, 4);
for ii = 1:numFrames
    fprintf('(%i%%) Saving video\n', round(100*ii/numFrames))
    writeVideo(diskLogger, data(:,:,:,ii));
end

close(diskLogger);
