function releaseDlzSession(session)
    % Releases the NI DAQ Resourse in a DLZ Session object
    release(session.highVoltageControlSession);
end