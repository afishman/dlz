function status(session)


% Tells you if the rig is closed or not by putting 100v through
v = 100;
data = trial(session, 1, @(t) v);

rigClosed = 0.8*v < mean(data.voltageChannel1);

if rigClosed
    disp('Rig is Closed')
else
    disp('Rig is Open')
end
