function plotCurrentLeakage(dirName)
%% List
filenames = listFiles(dirName, '*.mat');

%% Load
voltages = [];
current = [];

for i=1:length(filenames)
    trialData = load(filenames{i});
    
    voltages(end+1) = trialData.metaData.voltage;
    current(end+1) = trialData.data.currentChannel1(end);
    
end

%% Plot
hold on
grid on
plot(voltages/1000, current*1e6)
xlabel('Voltage (kV)')
ylabel('Current (uA)')