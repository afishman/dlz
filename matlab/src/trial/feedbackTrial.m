function [time, rawData] = feedbackTrial(session, voltageFun, tEnd)
%% Perform Trial
tStart = tic;
tPrevious = toc(tStart);
rawData = [];
time = [];
int = 0;
while tPrevious < tEnd   
    tCurrent = toc(tStart);
    
    % Read inputs from DAQ
    [inputDataLine, triggerTime] = inputSingleScan(session.highVoltageControlSession);    

    % Compute current state
    currentState = processRawData(session, tCurrent, inputDataLine);

    % Append to runtime data
    rawData = [rawData; inputDataLine];
    time = [time; tCurrent];
        
    % Update Outputs
    t = tCurrent;
    h = currentState.height;   
    v = currentState.voltageChannel1;
    dt = tCurrent - tPrevious;
    
    switch nargin(voltageFun)
        case 1
            voltage = voltageFun(t);
            
        case 2
            voltage = voltageFun(t,h);
            
        case 3
            voltage = voltageFun(t,h,v);
            
        case 4
            voltage = voltageFun(t,h,v,dt);
            
        otherwise
            error('voltageFun has too many inputs (must be f(t), f(t,h), f(t,h,v) or f(t,h,v,dt)')
    end
    
    % Update Outputs
    outputSingleScan(session.highVoltageControlSession, outputDataLine(session, voltage));
      
    % Set t_previous for next iteration
    tPrevious = tCurrent;
end
