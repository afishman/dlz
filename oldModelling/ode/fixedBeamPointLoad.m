function beam = fixedBeamPointLoad(L, EI, m, v)
    %% A fixed beam with a point load in the middle
    % L: Beam length
    % EI: flexural rigidity
    % m: point load mass
    % v: damping coefficient
    
    beam = struct;
    beam.L = L;
    beam.EI = EI;
    beam.m = m;
    
    % Damping
    if ~exist('v')
        v =0;
    end
    
    beam.v=v;