function session = dlzSession()

%% Session Parameters
session = struct();
session.controlFrequency = 1000;

session.hvMaxVoltage = 10000; % In Volts, per channel
session.hvMaxCurrent = 100e-6;% In Amps, per channel
session.daqIoMaxVolts = 10;
session.metersPerDaqVolt = 10e-3; % Laser Resolution
session.pascalsPerDaqVolt = 1; % For the pressure sensor

%% Calculate scaling factors
session.hvAmpsPerDaqVolt = session.hvMaxCurrent/session.daqIoMaxVolts;
session.hvVoltsPerDaqVolt = session.hvMaxVoltage/session.daqIoMaxVolts;

session.feedbackRateHz = 20;
session.deviceId = 'Dev2'; %TODO: Set this back to 'Dev2'

% Create Session
session.highVoltageControlSession = daq.createSession('ni');

%% NI DAQ Inputs
inputLabels =  {};

inputLabels{end+1} = 'laser';
laser = addAnalogInputChannel(session.highVoltageControlSession,session.deviceId,'ai0','Voltage');
laser.TerminalConfig = 'Differential';

inputLabels{end+1} = 'voltageInput1';
voltageMonitor1 = addAnalogInputChannel(session.highVoltageControlSession,session.deviceId,'ai1','Voltage');
voltageMonitor1.TerminalConfig = 'Differential';

inputLabels{end+1} = 'currentInput1';
curentMonitor1 = addAnalogInputChannel(session.highVoltageControlSession,session.deviceId,'ai2','Voltage');
curentMonitor1.TerminalConfig = 'Differential';

inputLabels{end+1} = 'voltageInput2';
voltageMonitor2 = addAnalogInputChannel(session.highVoltageControlSession,session.deviceId,'ai3','Voltage');
voltageMonitor2.TerminalConfig = 'Differential';

inputLabels{end+1} = 'currentInput2';
curentMonitor2 = addAnalogInputChannel(session.highVoltageControlSession,session.deviceId,'ai4','Voltage');
curentMonitor2.TerminalConfig = 'Differential';

inputLabels{end+1} = 'input5';
voltageMonitor3 = addAnalogInputChannel(session.highVoltageControlSession,session.deviceId,'ai5','Voltage');
voltageMonitor3.TerminalConfig = 'Differential';

inputLabels{end+1} = 'input6';
curentMonitor3 = addAnalogInputChannel(session.highVoltageControlSession,session.deviceId,'ai6','Voltage');
curentMonitor3.TerminalConfig = 'Differential';

% Pressure Added by Richard
inputLabels{end+1} = 'Pressure';
pressureMonitor = addAnalogInputChannel(session.highVoltageControlSession,session.deviceId,'ai18','Voltage');
pressureMonitor.TerminalConfig = 'Differential';

%% NI DAQ Outputs
outputLabels = {};

outputLabels{end+1} = 'channel1';
voltageProgramming1 = addAnalogOutputChannel(session.highVoltageControlSession,session.deviceId,'ao1','Voltage');

outputLabels{end+1} = 'channel2';
voltageProgramming2 = addAnalogOutputChannel(session.highVoltageControlSession,session.deviceId,'ao2','Voltage');

outputLabels{end+1} = 'channel3';
voltageProgramming3 = addAnalogOutputChannel(session.highVoltageControlSession,session.deviceId,'ao3','Voltage');

% Some housekeeping
session.highVoltageControlSession.Rate = session.controlFrequency;
outputSingleScan(session.highVoltageControlSession,[0 0 0]);

% Output session object
session.inputLabels = inputLabels;
session.outputLabels = outputLabels;