classdef Tests < matlab.unittest.TestCase
 
    methods(Test)
        function electricalStress(testCase)
            E = 1;
            L = 2;
            b = 3;
            d = 4;
            m = 5;
            em = 6;
            ei = 7;
            V = 8;
            nu = 9;
            y = 10;
            beam = cantilever(E, L, b, d, m, em, ei, V, nu);
            
            e0 = 8.85e-12;
            stress = em*e0*b*V^2 / (2*((em*d/ei + y)^2));
            
            assertEqual(testCase, stress, electricalStress(y, beam), ...
                'AbsTol', 1e-10 ...
            );
        end
    end
 
end