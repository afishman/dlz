function calibrate
% Take photo by triggering with the return key

% Number of snaps
if ~exist('n', 'var')
    n = 1;
end

% Attach to camera
vid = camera;
vid.FramesPerTrigger = 1;

% Preview
% preview(vid);
% pause(0.5); % HACK: Sometimes preview doesn't show without a pause

% Setup triggering
triggerconfig(vid, 'manual');
vid.TriggerRepeat = inf;
start(vid);

% Take it away
figure
while true
    % Wait for user input
%     x = input('Press return to trigger...');
    
    % Acquire snapshot
    trigger(vid);
    img = getdata(vid);
    [height, props] = detect(img);
    
    cla
    plotDetect(props);
    
end
