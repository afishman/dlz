function monitor
%% Heads up display of camera

% Attach to camera
settings = cameraSettings;

% Setup video
vid = camera;
vid.FramesPerTrigger = Inf;

% Heads-up display
preview(vid);