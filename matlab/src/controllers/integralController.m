function voltage = integralController(t, h, v, dt, K, hd, vMax)
    % K: integral gain
    % hd: desired height
    voltage = v + K*(hd-h)*dt;
    
    if voltage > vMax
        voltage = vMax;
    end
    
%     % For debugging
%     disp({'error', (hd-h)})
%     disp({'voltage rate', K*(hd-h)*dt})
%     disp({'dt', dt})
%     disp({'voltage', voltage})
%     disp({'v', v})
end