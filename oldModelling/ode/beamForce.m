function force = beamForce(h, beam)
    %% Reaction force for a beam
    L = beam.L;
    EI = beam.EI;

    a = L/2;
    b = a;

    % Assume a profile of fixed beam with a point load
    force = 48*EI*h / (b^2*(L+2*a) - 6*a*b^2);
    
end