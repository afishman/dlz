function sol = beamSim(beam, tSpan, h0)

if ~exist('h0', 'var')
    h0 = naturalDeflection(beam);
end

options = odeset('Events', @eventsFun);
sol = ode45(@(t,y)odeFun(t,y,beam), tSpan, [h0, 0], options);
