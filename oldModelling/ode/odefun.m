function dydt = odefun(t, y, beam)
    g = 9.807;

    % current height
    h = y(1);
    
    % Forces
    P = beam.m * g;
    f_d = y(2)*beam.v/beam.m;
    f_b = beamForce(h, beam);
    
    acc = (1/beam.m)*(f_b - P - f_d);
    
    % System of first order ODEs
    dydt = [
      y(2);
      acc;
    ];

end