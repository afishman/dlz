function voltage = scaleOutputVoltage(session, voltage)
    %% Voltage per channel fed to Daq, when splitting over two channels 
    voltage = 0.5*voltage/session.hvVoltsPerDaqVolt;