function time = settleTime(controller, data, tol)
    %% How long it take to approach steady state (stay bounded within a tolerance)

    % Default tolerance mm
    if ~exist('tol')
        tol = 0.5;
    end    

    % Steady state approach index
    index = find(abs(data.height-data.height(end))>tol, 1, 'last');
    
    % Time at approach
    time = data.time(index);
end