function v = electricalStress(beam, y)

%Helps-Taghavi Equation
A = beam.em * beam.e0 * beam.b * beam.V^2;
B = beam.em * beam.ti / beam.ei;

v = A ./ (2*(B + y).^2);