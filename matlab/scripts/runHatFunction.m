onTime = 2;
offTime =0.5;

tEnd = onTime + 2*offTime;
voltages = [8000, 8000, 8000, 8000, 8000, 8000];

for i=1:length(voltages)
    voltage = voltages(i);
    
    metadata=struct;
    metadata.votlage = voltage;

    fun = {
        @(t) hatFunction(t, voltage, onTime, offTime)
        @(t) hatFunction(t, voltage, onTime, offTime)
    };

%     fun = {
%         @(t) hatFunction(t, voltage, onTime, offTime)
%             @(t) 0
%         };
    
%     data = trial(session, tEnd, fun, 'metadata', metadata, 'video', true);
    data = trial(session, tEnd, fun, 'metadata', metadata);

    
    close all
    plotData(data);
    drawnow
    
end

% for i = 1:length(v_range)
%     v = @(t) hatFunction(t,v_range(i),onTime, offTime);
%     data = trial(session, v, tEnd);
% end
% close all;
% plotData(data);

