function [time, rawData] = queuedTrial(session, voltageFuns, t_end)
%% Pre-trial prep

% Queue Output Data
t = 0:1/session.highVoltageControlSession.Rate:t_end;

% Calculate voltage profiles for each channel
outputVoltages = zeros(length(t), length(session.outputLabels));
for i=1:length(voltageFuns)
    voltage = arrayfun(voltageFuns{i}, t);
    
    % Split voltage equally over two channels 
    channelVoltage = voltage/session.hvVoltsPerDaqVolt;
    outputVoltages(:, i) = channelVoltage;
end

queueOutputData(session.highVoltageControlSession, outputVoltages);

%% Perform Trial
[rawData,time] = session.highVoltageControlSession.startForeground;
