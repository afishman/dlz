clear all

EI = 1;
L = 1;
P = 1;

x = linspace(0, L, 100);

y = fixedBeamDeflection(x, P, L, EI);

plot(x,y)