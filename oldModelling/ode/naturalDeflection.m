function deflection = naturalDeflection(beam)
    P = beam.m*g;
    L = beam.L;
    a = L/2;
    EI = beam.EI;
    b = a;
    
    deflection = - P*(b^2*(L+2*a) - 6*a*b^2)/(48*EI);    
end
