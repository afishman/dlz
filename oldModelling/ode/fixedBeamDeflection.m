function deflection = fixedBeamDeflection(x, P, L, EI)
    a = L/2;
    b = L - a;

    % Deflection before point load
    deflection = (...
        + (b^2 * x.^3)*(L+2*a)/L.^3 ...
        - 3*a*b^2 * x.^2 / L^2 ...
    );

    % Deflection after point load
    if x>a
        deflection = deflection - (x-a).^3;
    end
    
    % Deflection after point load
    deflection = (P/(6*EI))*deflection;

end