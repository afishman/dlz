clear all
close all

% Response Data
massDeflections = [
    0.7 -0.88
    2.7, -0.99
    7.7, -1.55
    7.7, -1.66
    12.7, -3.02
    12.7, -2.92
    17.7 -4.35
    13.7, -3.39
    14.7, -3.54,
    3.7, -1.1,
    4.7, -1.12,
    5.7, -1.3057
    6.7, -1.53,
    8.7, -1.9094,
    10.7, -2.57,
    13.7, -3.2,
    14.7, -3.46
];

massDeflections(:,2) = -massDeflections(:,2)/1000;

beamResponse = array2table(massDeflections, 'VariableNames',{'weight', 'deflection'});

beamResponse = sortrows(beamResponse,'weight');
