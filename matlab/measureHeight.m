function height = measureHeight(session)

data = trial(session, 1, @(t) 0);

height = mean(data.height);