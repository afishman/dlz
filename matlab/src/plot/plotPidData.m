function plotPidData(data,hd)
%% Plot Output Data From Experiments
subplot(3,1,1)
hold on
labels = {};
labels{end+1}='voltage';
plot(data.time, data.voltage);
ylim([0, inf])

labels{end+1}='P';
plot(data.time, data.p);

labels{end+1}='I';
plot(data.time, data.i);

labels{end+1}='D';
plot(data.time, data.d);

labels{end+1}='Vs';
plot(data.time, data.vs);

legend(labels)
xlabel('time /s')
ylabel('voltage /KV')
grid on

subplot(3,1,2)
hold on
plot(data.time, data.height);
plot(data.time, data.hd);
legend({'height', 'set point'})
line([0 data.time(end)],[data.height(1) data.height(1)],'color','k');
xlabel('time /s')
ylabel('displacement /mm')
grid on

subplot(3,1,3)
hold on
plot(data.time, data.current);
xlabel('time /s')
ylabel('current /uA')
grid on

end