function nullclines(V, interactive, beam)

%% Nullclines for a beam
if ~exist('V', 'var')
    V = 350;
end
    
if ~exist('interactive', 'var')
    interactive = true;
end

%% Parameters
if ~exist('beam', 'var')
    beam = defaultBeam;
    beam.V = V;
    beam.nu = 0.5;
end

%% Solve for nullclines
h = linspace(0, 30e-3, 100);
hDdot = [];
for i = 1:length(h)     
    dydt = fzero(@(x) nullclineFun(h(i), x, beam), [-100, 100]);
    hDdot(end+1) = dydt;    
end

%% Plot
close all
hold on;grid on
fontsize = 15;
labelsize = 25;
legendsize = 15;
linewidth = 3;
set(gca, 'FontSize', fontsize)
xlabel('height ($h$)','Interpreter','latex', 'fontsize', labelsize)
ylabel('velocity ($\dot{h}$)','Interpreter','latex', 'fontsize', labelsize)

plot(h, hDdot, 'b', 'linewidth', linewidth)
plot(h, zeros(size(h)), 'r', 'linewidth', linewidth)
legend({'$\dot{h}$ nullcline', '$\ddot{h}$ nullcline'},'Interpreter','latex', 'fontsize', legendsize)
ylim([-0.02 0.02])
xlim([0 4e-3])
set(gca,'YDir','reverse')

%% Clicky
if ~interactive
    return
end

while true
    [h, hDot] = ginput(1);    
        
    options = odeset('Events', @eventsFun, 'RelTol',1e-8);
    sol = ode45(@(t,y)odeFun(t,y,beam), [0 10], [h, hDot], options);
    
    plot(sol.y(1,:), sol.y(2,:), 'g','HandleVisibility','off')
    quiver(sol.y(1,1:end-1), sol.y(2,1:end-1),...
        diff(sol.y(1,:)), diff(sol.y(2,:)), ...
        'g','HandleVisibility','off')
end