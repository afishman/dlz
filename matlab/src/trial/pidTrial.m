function [time, rawData, pidData] = pidTrial(session, pidController, tEnd)
%% Perform Trial
tStart = tic;
tPrevious = toc(tStart);
rawData = [];
time = [];
pidData = [];
int = 0;
while tPrevious < tEnd   
    tCurrent = toc(tStart);
    
    % Read inputs from DAQ
    [inputDataLine, triggerTime] = inputSingleScan(session.highVoltageControlSession);    

    % Compute current state
    currentState = processRawData(session, tCurrent, inputDataLine);
        
    % Update Outputs
    t = tCurrent;
    h = currentState.height;   
    v = currentState.voltage;
    dt = tCurrent - tPrevious;
    
    voltage = pidController.update(h, t, dt);
    
    p = pidController.propotionalVoltage;
    i = pidController.integralVoltage;
    d = pidController.derivativeVoltage;
    vs = pidController.Vs;
    hd = pidController.currentHd(t);
    
    currentPidData = array2table([p,i,d,vs,hd], 'VariableName', {'p','i','d', 'vs', 'hd'});
   
    % Append to runtime data
    rawData = [rawData; inputDataLine];
    time = [time; tCurrent];
    pidData = [pidData; currentPidData];
    
    % Update Outputs
    outputSingleScan(session.highVoltageControlSession, outputDataLine(session, voltage));
      
    % Set t_previous for next iteration
    tPrevious = tCurrent;
end
