function plotData(data)
%% Plot Output Data From Experiments
subplot(3,1,1)
hold on
plot(data.time, data.voltageChannel1/1000);
plot(data.time, data.voltageChannel2/1000);
legend('Channel 1', 'Channel 2');
xlabel('time /s')
ylabel('voltage /KV')
grid on

subplot(3,1,2)
hold on
plot(data.time, data.height*1000);
xlabel('time /s')
ylabel('displacement /mm')
grid on

subplot(3,1,3)
hold on
plot(data.time, data.currentChannel1*1e6);
plot(data.time, data.currentChannel2*1e6);
legend('Channel 1', 'Channel 2');
xlabel('time /s')
ylabel('current /uA')
grid on

end