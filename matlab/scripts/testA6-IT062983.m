voltage = 7000;

%% Hat Function
offTime = 1;
onTime = 5;

% duration = 2*onTime+2*offTime;
% voltageFuns = {
% 	@(t) hatFunction(t, voltage, onTime, offTime)
% 	@(t) hatFunction(t, voltage, onTime, onTime+2*offTime)
% };

duration = onTime+offTime;
voltageFuns = {
	@(t) hatFunction(t, voltage, onTime, offTime)
	@(t) hatFunction(t, voltage, onTime, offTime)
};

% duration = onTime+offTime;
% voltageFuns = {
% 	@(t) 0
% 	@(t) hatFunction(t, voltage, onTime, offTime)
% };


%% Plot
close all
plotVoltageFun(voltageFuns, duration)

%% Run
for i=1:1
    data = trial(session, duration, voltageFuns);
end
figure
plotData(data)