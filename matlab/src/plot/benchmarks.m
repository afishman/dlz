function b = benchmarks(controller, data)
    % Collect a variety of benchmarks and put into struct
    b = struct;
    b.hd = controller.hd;
    
    % Benchmarks
    b.ssErr = steadyStateError(controller, data);
    b.overshoot = overshoot(controller, data);
    b.riseTime = riseTime(controller, data);    
    b.settleTime = settleTime(controller, data);
    b.meanSquaredError = meanSquaredError(controller, data);
    
    % Data about the controller
    b.p = controller.Kp;
    b.i = controller.Ki;
    b.d = controller.Kd;    
end