function [data, rawDataTable] = processRawData(session, time, rawData)
% Concatenate raw data into a table
rawDataTable = array2table([time, rawData],  'VariableNames', [{'time'}, session.inputLabels]);

% Data for the table
outputData = [];
outputLabels = {};

% Time
outputLabels{end+1} = 'time';
outputData = [outputData, time];
  
% Height
scaledHeight = session.metersPerDaqVolt.*rawDataTable{:, {'laser'}};
outputLabels{end+1} = 'height';
outputData = [outputData, scaledHeight];

% Voltage
outputLabels{end+1} = 'voltageChannel1';
v1 = session.hvVoltsPerDaqVolt.*rawDataTable{:, {'voltageInput1'}};
outputData = [outputData, v1];

outputLabels{end+1} = 'voltageChannel2';
v2 = session.hvVoltsPerDaqVolt.*rawDataTable{:, {'voltageInput2'}};
outputData = [outputData, v2];

% Current
c1 = session.hvAmpsPerDaqVolt*rawDataTable{:, {'currentInput1'}};
outputLabels{end+1} = 'currentChannel1';
outputData = [outputData, c1];

c2 = session.hvAmpsPerDaqVolt*rawDataTable{:, {'currentInput2'}};
outputLabels{end+1} = 'currentChannel2';
outputData = [outputData, c2];

% Pressure 
p = session.pascalsPerDaqVolt*rawDataTable{:, {'Pressure'}};
outputLabels{end+1} = 'Pressure';
outputData = [outputData, p];

% Form Table
data = array2table(outputData, 'VariableNames', outputLabels);

end