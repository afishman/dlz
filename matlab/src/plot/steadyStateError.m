function err = steadyStateError(pidController, data)
    % The difference between the end height and desired height

    %Height
    hd = pidController.hd;   
    err = abs(hd - data.height(end));
end