
onTime = 2;
offTime = 0;
restTime = 0;

voltages = 8000;

numCycles = 5;
 
cycleTime = 2*offTime+restTime+2*onTime;
tEnd = cycleTime*numCycles;

% voltages = [5000, 6500, 8000];
for i=1:length(voltages)
    voltage = voltages(i);

    fun = {
        @(t) hatFunction(mod(t, cycleTime), voltage, onTime, offTime+restTime+onTime)
        @(t) hatFunction(mod(t, cycleTime), voltage, onTime, offTime)       
    };

    plotVoltageFun(fun, tEnd)
    data = trial(session, tEnd, fun);
% 
%     if i==cycles
%         data = trial(session, tEnd, fun, 'metadata', metadata, 'video', true);
%     else
%         data = trial(session, tEnd, fun, 'metadata', metadata);        
%     end    
end
% close all
% plotData(data);
% drawnow