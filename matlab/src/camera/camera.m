function vid = camera
%% Connect to a Point Grey Grasshopper camera

% Reset Devices
imaqreset

% Attach to camera
settings = cameraSettings;

% Setup video
vid = videoinput('pointgrey', 1, settings.bayer);

// vid.ROIPosition = settings.roi;

src = getselectedsource(vid);
src.FrameRate = settings.FrameRate;
