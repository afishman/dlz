function vline(x,fmt)
limits = ylim(gca);

if ~exist('fmt')
   fmt = 'k:';
end

hold on
plot([x,x], limits, fmt);
