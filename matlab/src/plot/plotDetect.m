function plotDetect(props)
%% Plots
% Original
hold on
title('Original')
imshow(props.img)
hold on

for i=1:length(props.lidContours)
    plot(props.lidContours{i}(:,2), props.lidContours{i}(:,1), 'c', 'LineWidth', 2)
end
plot(props.lid(:,2), props.lid(:,1), 'g', 'LineWidth', 2)

for i=1:length(props.beamContours)
    plot(props.beamContours{i}(:,2), props.beamContours{i}(:,1), 'c', 'LineWidth', 2)
end
plot(props.beam(:,2), props.beam(:,1), 'y', 'LineWidth', 2)

plot([0, size(props.img, 2)], [props.baseHeight, props.baseHeight], 'k--', 'LineWidth', 2)
%plot([0, size(img, 2)], [beam_height, beam_height], 'k--', 'LineWidth', 2)
plot([props.beamX, props.beamX], [props.beamHeight, props.baseHeight], 'k--', 'LineWidth', 2)
