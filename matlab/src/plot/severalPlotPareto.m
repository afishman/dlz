figure;

subplot(1,3,1)
title('overshoot VS ssErr')
plotPareto('./A2/OptimizePID_3.18g_hd-20mm', 'ssErr', 'overshoot')

subplot(1,3,2)
title('riseTime VS ssErr')
plotPareto('./A2/OptimizePID_3.18g_hd-20mm', 'ssErr', 'riseTime')

subplot(1,3,3)
title('settleTime VS ssErr')
plotPareto('./A2/OptimizePID_3.18g_hd-20mm', 'ssErr', 'settleTime')