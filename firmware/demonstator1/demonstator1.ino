#include "pidController.h"
#include <Wire.h>
#include <Adafruit_MCP4725.h>

Adafruit_MCP4725 DAC;

PidController* CONTROLLER;

// From CB-101 Datasheet
// https://www.xppower.com/portals/0/pdfs/SF_CB_Series.pdf
float HIGH_VOLTAGE_PSU_OUTPUT_VOLTS = 10000;
float HIGH_VOLTAGE_PSU_OUTPUT_AMPS =  0.001;

int VOLTAGE_MONITOR_PIN = A10;
int CURRENT_MONITOR_PIN = A9;

int IR_SENSOR_SAMPLES_PER_READING = 25;
int IR_SENSOR_PIN = A2;

// For Voltage Feedback
int LED_PIN = 2;

// Time of the last iteration in seconds
float T_PREV_S;

float PGAIN = 1;
float IGAIN = 1;
float DGAIN = 1;
float VMAX = 10000;
float VS = 1000;


void setup(void) {
  Serial.begin(115200);

  // For Adafruit MCP4725A1 the address is 0x62 (default) or 0x63 (ADDR pin tied to VCC)
  DAC.begin(0x62);

  // Initialise PidController
  float h = readIrSensorMm();
  float hd = 1; // TODO: link to pot

  CONTROLLER = new PidController(h, hd, PGAIN, IGAIN, DGAIN, VMAX, VS);

  // Initialise time
  T_PREV_S = time_s();
}

float time_s()
{
  return ((float)millis())/1000;
}

void loop() 
{  
      // Timekeeping
      float t = time_s();
      float dt = t - T_PREV_S;
      T_PREV_S = t;

      // Read
      float h = readIrSensorMm();

      // Control
      float voltage = CONTROLLER->update(h, t, dt);

      // Update
      //setHv(voltage);
      voltage = 10000*0.5*(sin(2*PI*t)+1);
      voltage = 5000;
      
      setHv(voltage);

      // Read Voltage Monitor
      float voltage_monitor = readVoltageMonitor();
      

      analogWrite(LED_PIN, (int) (255*voltage_monitor/voltage));
      
      // Print
      // Format: <t>, <height>, <voltage monitor>, <current monitor>, <set voltage>, <p>, <i>, <d>
      String message = "";
      message += String(t, 3);
      message += ",";
      message += String(h);
      message += ",";
      message += String(readVoltageMonitor());
      message += ",";
      message += String(readCurrentMonitor(),6);
      message += ",";
      message += String(voltage);
      message += ",";
      message += String(CONTROLLER->propotionalVoltage());      
      message += ",";
      message += String(CONTROLLER->integralVoltage());      
      message += ",";
      message += String(CONTROLLER->derivativeVoltage());

      Serial.println(message);

      delay(30);
}

float readVoltageMonitor()
{
    return (HIGH_VOLTAGE_PSU_OUTPUT_VOLTS/1023.0) * ((float)analogRead(VOLTAGE_MONITOR_PIN));
}

float readCurrentMonitor()
{
    return (HIGH_VOLTAGE_PSU_OUTPUT_AMPS/1023.0) * ((float)analogRead(CURRENT_MONITOR_PIN));
}

// Set the high voltage. Saturates at 0 and HIGH_VOLTAGE_PSU_OUTPUT
void setHv(float voltage)
{
    // Ensure voltage is in an allowable range
    if(voltage > HIGH_VOLTAGE_PSU_OUTPUT_VOLTS)
    {
       voltage = HIGH_VOLTAGE_PSU_OUTPUT_VOLTS;
    }
    else if(voltage < 0)
    {
       voltage = 0;
    }

    // Set Hv
    uint16_t dacLevel = (uint16_t) 4095*voltage/HIGH_VOLTAGE_PSU_OUTPUT_VOLTS;
    
    DAC.setVoltage(dacLevel, false);
}

// Take a reading from the Sharp IR GP2Y0A41SK0F in mm
// This is a slimmed down version of: https://github.com/guillaume-rico/SharpIR/blob/master/SharpIR.cpp
// Runs at around 300Hz on an arduino mega with 25 samples
float readIrSensorMm()
{
    // Read Samples
    int samples[IR_SENSOR_SAMPLES_PER_READING];
    for (int i=0; i<IR_SENSOR_SAMPLES_PER_READING; i++)
    {
       samples[i] = analogRead(IR_SENSOR_PIN);
    }

    // Sort
    qsort(samples, IR_SENSOR_SAMPLES_PER_READING, sizeof(int), ascendingCompareFun);

    // Choose the middle value as the reading
    int val = samples[IR_SENSOR_SAMPLES_PER_READING / 2];
  
    // Remap
    // Thanks: https://www.instructables.com/id/How-to-Use-the-Sharp-IR-Sensor-GP2Y0A41SK0F-Arduin/
    float volts = val*(5.0/1024.0);  // value from sensor * (5/1024)
    return 130*pow(volts, -1); // worked out from datasheet graph
}

// Ascending function for qsort
// Thanks: https://www.tutorialspoint.com/c_standard_library/c_function_qsort.htm
int ascendingCompareFun (const void * a, const void * b) {
   return ( *(int*)a - *(int*)b );
}

