// Hat Function Controls
float ON_TIME = 5;
float OFF_TIME = 2;

// Relay Pin Mapping
int POWER_PIN1 = 53;
int GROUND_PIN2 = 47;

int POWER_PIN2 = 51;
int GROUND_PIN1 = 49;

int LOOP_DELAY_MS = 30;

void setup() {
    // Init Pins
    pinMode(POWER_PIN1,OUTPUT);
    pinMode(POWER_PIN2,OUTPUT);
    pinMode(GROUND_PIN1,OUTPUT);
    pinMode(GROUND_PIN2,OUTPUT);

    // Turn of initially
    setPins(false, false);

    // Serialise
    Serial.begin(115200);
}

void loop() {
    // Serial takeover mode if a message is received
    if (Serial.available())
    {
        while(processSerial()) {}
    }
  
    // Hat Pattern
    float period = 2*ON_TIME + 2*OFF_TIME;
    float t = fmod(time_s(), period);

    bool channel1 = t<ON_TIME;
    bool channel2 = (ON_TIME+OFF_TIME) <= t && t < (2*ON_TIME + OFF_TIME);

    setPins(channel1, channel2);

    // Print
    String ch1 = channel1 ? "1" : "0";
    String ch2 = channel2 ? "1" : "0";
    Serial.println(ch1 + "," + ch2);

    // Sleepy-time
    delay(LOOP_DELAY_MS);
}

// Read a serial message
// To update pins give a 2 charachter binary string ('1' for on, '0' for off)
// Returns to true if message is a quit command, false otherwise
bool processSerial() 
{
    if(!Serial.available())
    {
        return true;
    }

    // Read from serial
    String serial_string = Serial.readStringUntil('\n');

    // Quit Command
    if(serial_string == "q" || serial_string == "quit")
    {
        return false;
    }

    // Check string length
    if(!serial_string.length() == 2)
    {
        Serial.println("Expect a 2 charachter binary string");
        return true;
    }

    // Parse channel states
    bool channel1 = serial_string[0] == '1';
    bool channel2 = serial_string[1] == '1';

    // Update
    setPins(channel1, channel2);

    // Print
    Serial.println("done"); 
    return true;
}

// Update a pair of relay channels
void setPins(bool channel1, bool channel2)
{
    setPin(channel1, POWER_PIN1, GROUND_PIN1);
    setPin(channel2, POWER_PIN2, GROUND_PIN2);   
}

// Update the state of a channel. true set power high and ground low. False does the opposite
void setPin(bool state, int powerPin, int groundPin)
{
    if (state) 
    {
        // "When they go low, you go high", Michelle Obama
        digitalWrite(groundPin, LOW);
        digitalWrite(powerPin, HIGH);          
    }
    else
    {
        // "When they go high, you go low", ....
        digitalWrite(groundPin, HIGH);
        digitalWrite(powerPin, LOW);
    }
}

// Time in seconds
float time_s()
{
    return ((float)millis())/1000;
}

