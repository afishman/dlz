function [freq, amp] = realFft(t, x)

if (length(t) ~= length(x))
    error('signals must be same length');
end

%Number of samples
numSamples = length(t);
if mod(numSamples, 2) == 1
    numSamples = numSamples+1;
end

%Interpolate the signal
tInterp = linspace(t(1), t(end), numSamples);
xInterp = interp1(t, x, tInterp);

% Detrend
xDetrend = detrend(xInterp, 'constant');

% Perform fft
xFft = fft(xDetrend);
xFft = xFft / (2*length(numSamples));

% Frequencies
Fs = numSamples/(tInterp(end)-tInterp(1));
freq = Fs*(0:(numSamples/2)-1)/numSamples;

% Amplitude Spectrum
amp = abs(xFft(1:length(xFft)/2));
