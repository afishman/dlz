function hDot = rootFun(x, beam)
% Use for finding roots to the DLZ Equation

dydt = odeFun(0, [x, 0], beam);
hDot = dydt(2);