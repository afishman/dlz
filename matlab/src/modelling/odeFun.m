function dydt = odeFun(t, y, beam)
    h = y(1);
    hDot = y(2);

    % Forces
    V = beam.nu*hDot;
    R = h*3*beam.EI / (beam.L^3);
    P = beam.m*beam.g;
    
    W = integral(@(x) electricalStress(beam, deflection(beam, x, h)), 0, beam.L);
    
    %W = integral(@(x) electricalStressEpsilon(beam, deflection(beam, x, h)), 0, beam.L);
    
    % Acceleration
    acc = P - V - R - W;
    acc = acc/beam.m;
    
    % ODE components
    dydt = [
        y(2)
        acc
    ];
end