voltages = 0:500:4000;


%% Plot
close all

%% Run
for i=1:length(voltages)  
    fprintf('Trial %i/%i\n', i, length(voltages))
    
    offTime = 5;
    onTime = 30;
    
    voltage = voltages(i);
    
    metadata = struct;
    metadata.voltage = voltages(i);
    
    duration = onTime+offTime;
    voltageFuns = {
        @(t) stair(t, onTime, voltage)
        @(t) stair(t, onTime, voltage)
    };

    close all
    plotVoltageFun(voltageFuns, duration)

    data = trial(session, duration, voltageFuns, 'video', true, 'metadata', metadata, 'filename' ,[num2str(voltage), 'v']);
end
close all
plotData(data)