%% Load
close all
directory = '/Users/aaron/Documents/DLZ/cantilever/run3/';
load([directory, 'beam.mat']);
beam.ti = 2*0.8e-3;
beam.nu = 60;
%beam.ei = 0.7;
%beam.ti = 2*0.8e-3;

files = listFiles(directory, '*V.mat');

%% Get voltage and end height for each file
hInf = [];
voltage = [];

experiments = {};

figure; hold on; grid on
for i=1:length(files)
    filepath = files{i};
    experiment = load(filepath);
    experiments{end+1} = experiment;

    voltage(end+1) = experiment.metaData.voltage;
    
    idx = find(experiment.data.time >= 59, 1);
    hInf(end+1) = experiment.data.height(idx);
    
    
end

%% Rezero
rezero = max(hInf);
for i=1:length(files)
    experiments{i}.data.height = experiments{i}.data.height - rezero;
end
hInf = hInf - rezero;

%% Plot results
labels = {};
for i=1:length(files)
    labels{end+1} = sprintf('%.0fV', experiments{i}.metaData.voltage);
    
    plot(experiments{i}.data.time, experiments{i}.data.height);
end
legend(labels);


%% Effective Force for a lumped mass
hInfAbs = abs(hInf);
k = beam.m*g/mean(experiments{1}.data.height(1:500));
fe = beam.m*beam.g - k*hInfAbs;

% figure; hold on; grid on;
% xlabel('h')
% ylabel('fe')
% plot(beam.m*g - k*hInfAbs);

%%
% Total electrical force
%HACK
%beam.e0 = 0.15*8.85418782e-12;


%% Plot beam profile
eForce = [];
for i=1:length(hInf)
    beam.V = voltage(i);    
    eForce(i) = integral(@(x) electricalStress(beam, deflection(beam, x, -hInf(i))), 0, beam.L);
    
    %beam.eEff = 0.8;
    %eForce(i) = integral(@(x) electricalStressEpsilon(beam, deflection(beam, x, -hInf(i))), 0, beam.L);
end

figure; hold on; grid on;
title('h inf')
xlabel('voltage')
ylabel('Force (N)')
plot(voltage, fe);
plot(voltage, eForce);
legend('data', 'model')
ylim([0, 0.03])

%% Comparison
figure; grid on; hold on; title('Sim vs Experiment')
%for i=1:2:length(experiments)
labels = {};
for i=1:4:length(experiments)
    % Plot experiment
    experiment = experiments{i};
    
    voltage = experiment.metaData.voltage;
    
    beam.V = voltage;
    sol = beamSim(beam, [0, 10], -hInf(1));
    
    labels{end+1} = sprintf('%iV', beam.V);
    plot(experiment.data.time, experiment.data.height)
    plot(sol.x, -sol.y(1,:), 'k', 'HandleVisibility','off')
    
end
legend(labels)


%% Beam sol
beam.V = 1900;
sol = beamSim(beam, [0, 10], -hInf(1));

figure
plot(sol.x, sol.y(1,:))
title('Ode')

figure 
nullclines(0,0,beam);

%% Energy
beam.m = 17.7e-3;
P = beam.m*g;
T = P*(hInf - min(hInf));
U = 3*beam.EI*(hInf - min(hInf)).^2 / (beam.L^3);
E = T-U;

figure; hold on; grid on
plot(voltage, T);
plot(voltage, U);
plot(voltage, E);
xlabel('voltage')
ylabel('energy (J)')
legend('T', 'U', 'E')

C = 2*E./(voltage.^2);
figure; hold on
title('cap')
plot(voltage, C)

%%

figure; hold on
title('H inf - cap')
xlabel('hinf')
ylabel('C')
plot(hInf(1:18), C(1:18))