function rect = roi
%% Click and drag to select a ROI. The dimensions printed here can be used to set vid.ROIPosition

% Reset Devices
imaqreset

% Attach to camera
settings = cameraSettings;

% Setup video
vid = videoinput('pointgrey', 1, settings.bayer);
vid.FramesPerTrigger = 1;

src = getselectedsource(vid);
src.FrameRate = settings.FrameRate;

triggerconfig(vid, 'manual');
vid.TriggerRepeat = inf;
start(vid);

pause(0.5);


rect = [0 0 0 0]
while true
    gca
    
    % Acquire snapshot
    trigger(vid);
    img = getdata(vid);
    
    imshow(img)
    hold on

    rectangle('Position',rect);
    rect = getrect;    
    
    fprintf('[%i %i %i %i]\n', round(rect(1)), round(rect(2)), round(rect(3)), round(rect(4)))
end