%% Beam
beam = defaultBeam;

V = [0:1:418, 419:.01:420, 420:1:500];

root1 = [];
root2 = [];

rootSearchPts = 1000;

for i=1:length(V)
    fprintf('%.0f%%\n', 100*i/length(V))
    
    beam.V = V(i);
    
    % Brute force root search
    vals = [];
    x = linspace(0, 30e-3, rootSearchPts);
    for j=x
        vals(end+1) = rootFun(j, beam);
    end
    
    crossings = sort(find(diff(sign(vals))~=0));   
    
    switch length(crossings)
        case 0
              root1(end+1) = NaN;
              root2(end+1) = NaN;
        case 1
              root1(end+1) = x(crossings(1));
              root2(end+1) = NaN;
        case 2
              root1(end+1) = x(crossings(2));
              root2(end+1) = x(crossings(1));
            
        otherwise
            error('%i roots found', lenght(crossings))
    end
    
    
    
end

%% Find regimes
numRoots = ~isnan(root1) + ~isnan(root2);
regimeChange1 = V(find(numRoots==2, 1, 'first'));
regimeChange2 = V(find(numRoots==0, 1, 'first'));

%% Plot

close all
hold on
grid on

fontsize = 15;
labelsize = 25;
legendsize = 15;
linewidth = 3;
set(gca, 'FontSize', fontsize)

plot([regimeChange1, regimeChange1], [0 30e-3], 'k--','HandleVisibility','off')
plot([regimeChange2, regimeChange2], [0 30e-3], 'k--','HandleVisibility','off')

x = linspace(0, regimeChange1, 100);
plot(x, zeros(size(x)), 'k--', 'linewidth', linewidth,'HandleVisibility','off')

x = linspace(regimeChange1, V(end), 100);
plot(x, zeros(size(x)), 'k', 'linewidth', linewidth)

plot(V, root1, 'linewidth', linewidth)
plot(V, root2, '--', 'linewidth', linewidth)

set(gca,'YDir','reverse')



legend('Zipped Stability', 'Stable Node', 'Unstable Node')

ylim([0 30e-3])
xlabel('V','fontsize', labelsize)
ylabel('h (mm)','fontsize', labelsize)