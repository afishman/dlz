function hd = hStaircase(t)

heights = [-28, -20, 2];
times = [0, 5, 15];

index = find(t<times, 1, 'first');

if isempty(index)
    hd = heights(end);
else
    hd = heights(index-1);
end
