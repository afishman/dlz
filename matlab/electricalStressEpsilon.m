function v = electricalStressEpsilon(beam, y)

v = 0.5* beam.e0 * beam.eEff * beam.b * (beam.V.^2) ./ ((y+beam.d).^2);
