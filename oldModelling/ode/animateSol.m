function animateSol(beam, t, y)

% Height
h = y(:,1);

for i=1:length(t)
    % Calculate Beam  Profile
    [x,y] = beamProfile(h(i), beam);
    
    % Plot
    plot(x, y, 'b')
    hold off
    title(sprintf('t=%.1f', t(i)))
    axis([0 beam.L min(h) max(h)])
    grid on
    xlabel('x (m)')
    ylabel('y (m)')
    
    % Chill
    pause(.1)
end