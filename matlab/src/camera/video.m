function video(t, filepath)
%% Record a video
%% t - double - Record length in seconds 
%% filepath - str (optional)

vid = camera;
vid.FramesPerTrigger = Inf;

% Heads-up display
preview(vid);
pause(0.5);

% Show video
start(vid);
pause(t);
stop(vid);
stoppreview(vid);

closepreview

% Write to disk
if ~exist('filepath', 'var')
    filepath = [datestr(now,'dd-mmm-yy--HH-MM-SS'), '.avi'];
end
disp(['saving as ', filepath]);

render(vid, filepath)
