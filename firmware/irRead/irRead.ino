int IR_SAMPLES_PER_READING = 25;
int IR_PIN = A0;

void setup() {
  Serial.begin(115200);
}

void loop() {
  int start = millis();
  float cm = readSharpIr(IR_PIN, IR_SAMPLES_PER_READING);
  return;

}

//TODO: Should this be in mm?
// Take a reading from the Sharp IR GP2Y0A41SK0F 
// This is a slimmed down version of: https://github.com/guillaume-rico/SharpIR/blob/master/SharpIR.cpp
// Runs at around 300Hz on an arduino mega with 25 samples
float readSharpIr(int pin, int num_samples)
{
  // Read Samples
  int samples[num_samples];
  for (int i=0; i<num_samples; i++)
  {
     samples[i] = analogRead(pin);
  }

  // Sort
  qsort(samples, num_samples, sizeof(int), cmpfunc);

  // Choose the middle value as the reading
  int val = samples[num_samples / 2];
  
  // Remap
  // Thanks: https://www.instructables.com/id/How-to-Use-the-Sharp-IR-Sensor-GP2Y0A41SK0F-Arduin/
  float volts = val*(5.0/1024.0);  // value from sensor * (5/1024)
  return 13*pow(volts, -1); // worked out from datasheet graph
}


// elements descendingly
// Thanks: https://www.tutorialspoint.com/c_standard_library/c_function_qsort.htm
int cmpfunc (const void * a, const void * b) {
   return ( *(int*)a - *(int*)b );
}

