clear all
close all

dampedBeamExperiment = load('dampedBeam.mat');

L = dampedBeamExperiment.beamLength;
% m = (dampedBeamExperiment.mass)*0.001;
m = 23.4*0.001;
v = 0.003; % TODO: Curve Fit

% Predicted Flexural Rigidity from data sheet
E = dampedBeamExperiment.steelStiffness;
I = (1/12)*dampedBeamExperiment.beamWidth*(dampedBeamExperiment.beamThickness*0.5)^3;

EI_pred = E*I;

% Back-calculate effective flexural rigidity from data
EI_eff = -(m*g*L^3) / (192*dampedBeamExperiment.data.height(end)*0.001);

fprintf('Predicted Flexural Rigidity: %.0e\n', EI_pred)
fprintf('Empirical Flexural Rigidity: %.0e\n', EI_eff)

%%
beam = fixedBeamPointLoad(L, EI_eff, m, v);

% Solve
options = odeset('RelTol',1e-8);
[t,y] = ode45(@(t,y)odefun(t,y,beam), [0,30], [0,0], options);

% Plot Solution
labels = {};

labels{end+1} = 'Model';
plot(t,y(:,1))

hold on

labels{end+1} = 'Experiment';
plot(dampedBeamExperiment.data.time-2.3070, 0.001*dampedBeamExperiment.data.height)

legend(labels)
xlabel('Time (s)')
ylabel('Deflection (m)')

% Fft
tMax = 10;

figure
startIndex = 2372;
endIndex = find(dampedBeamExperiment.data.time>=dampedBeamExperiment.data.time(startIndex)+tMax, 1 ,'first');
tExpFft = dampedBeamExperiment.data.time(startIndex:endIndex);
xExpFft = dampedBeamExperiment.data.height(startIndex:endIndex);

[freq, amp] = realFft(tExpFft, xExpFft);
plot(freq, amp/length(amp));
hold on

startIndex = 1;
endIndex = find(t>=t(startIndex)+tMax, 1 ,'first');
[freq, amp] = realFft(t(startIndex:endIndex), y(startIndex:endIndex,1));
plot(freq, amp/length(amp));

legend('Experiment', 'Simulation')
grid on
xlabel('Frequency')
ylabel('amp')