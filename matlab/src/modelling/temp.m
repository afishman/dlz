% BEAMZ: http://structx.com/Beam_Formulas_016.html
% https://www.engineeringtoolbox.com/beams-fixed-both-ends-support-loads-deflection-d_809.html
% https://www.amesweb.info/Vibration/Natural-Frequency-Uniform-Beam-Both-Ends-Fixed.aspx

h = 3.121e-3;

m = 3*h*beam.EI/(g*beam.L^3)