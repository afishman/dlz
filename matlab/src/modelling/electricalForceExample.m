close all

%% Default Beam
beam = defaultBeam;
beam.V = 3000;

% Electrical response after a 0V rest
h = 10e-3;
x = linspace(0, beam.L, 100);
y = deflection(beam, x, h);
s = electricalStress(beam, y);
P = beam.m*beam.g;

% Total electrical force
force = @(H) integral(@(x) electricalStress(beam, deflection(beam, x, H)), 0, beam.L);

forces = [];
heights = linspace(0, 50, 1000);
for i=heights
    forces(end+1) = force(i);
end

%% Plot
% Steady state against deflection
figure; hold on; grid on
title('Electrical Stress')
xlabel('x (mm)')

yyaxis left
plot(1000*x, s/1000);
ylabel('Stress (N/mm)')

yyaxis right
plot(1000*x, 1000*y);
ylabel('Deflection (mm)')

%Force against height
figure; hold on; grid on
title('Electrical Force Response')
xlabel('h (mm)')
ylabel('F (N)')
ylim([0 0.3])
plot(heights, forces);