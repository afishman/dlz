function err = meanSquaredError(controller, data)
    %% Difference between the target height and the desired height
    
    hd = controller.hd;    
    err = mean((data.height - hd).^2);
end