function data = demo1Trial(port, tEnd, dirName)
if ~exist('port', 'var')
    port = '/dev/tty.usbmodem1411';
end

arduino = serial(port, 'BaudRate',115200);
fopen(arduino);
onCleanup(@()fclose(arduino));


%% Perform Trial
tStart = tic;
t = 0;
data = [];
while t<tEnd
    t = toc(tStart);
    
    % Read Line and parse
    str = fgetl(arduino);
    nums = str2double(strsplit(str,','));
    
    % Create table line
     %<t>, <height>, <voltage monitor>, <current monitor>, <set voltage>, <p>, <i>, <d>
    dataline =  array2table([nums t], 'VariableName', {
         'time', ...
         'height', ...
         'voltage', ...
         'current', ...
         'control_voltage', ...
         'p', ...
         'i', ...
         'd', ...
         'tMatlab' ...
    });
    
    data = [data; dataline];
end

% Save to file
trialData = struct;
trialData.tEnd = tEnd;
trialData.data = data;

if exist('dirName', 'var')
    saveTrial(trialData, dirName);
else
    saveTrial(trialData);
end