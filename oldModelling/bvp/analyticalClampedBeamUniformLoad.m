function [x,y]=a_cb_u(L, EI, w)
%% A simply supported beam of length L with a uniform load w

x = linspace(0, L, 100);
y = ((w.*x.^2)./(24.*EI)).*(L-x).^2;
