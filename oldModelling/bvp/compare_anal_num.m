close all

% Params
L = 1;
EI = 1;

% Analytical Result
[x,y] = analyticalClampedBeamUniformLoad(L, EI, 1);
plot(x,y)
hold on

% Numerical Result Result
[x,y] = numericalClampedBeam(L, EI, @(x)1);
plot(x,y);

legend('Analytical','Numerical')

% Mean Error
