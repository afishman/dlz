%hd = -20;
hi = -34.5;
hdi = hi+0.5;
hd = @(t) hRampcase(hdi,t);
% hd = -25;
hd = @sineyTime;
vC = 6000;

kp = 1000;
ki = 60;
kd = 0;

vMax = 9000;
vs = vC - kp*(hdi-hi);

tEnd = 90;
linHeight = false;

controller = PidController(session, hd, kp, ki, kd, vMax, vs, linHeight);
% controller = @(t) 2000*sin((1/3)*2*pi*t)+ 7000;

data = trial(session, controller, tEnd);
close all;
plotPidData(data,hd);