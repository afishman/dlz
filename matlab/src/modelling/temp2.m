close all

t = linspace(0, 5, 100);
x = sin(2*2*pi*t);

plot(t,x);
xlabel('t')
ylabel('x')

figure
[freq, amp] = realFft(t,x);
plot(freq, amp)
xlabel('freq')
ylabel('amp')
