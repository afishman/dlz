voltage = 5000;

%% Hat Function
offTime = 5;
onTime = 5;
% duration = onTime*3;
% duration = 3*onTime+2*offTime;
% voltageFuns = {
% 	@(t) hatFunction(t, 5000, onTime, offTime) + hatFunction(t, 6500, onTime, 0.1+onTime+offTime) + hatFunction(t, 8500, onTime, 0.2+2*onTime+offTime)
%  	@(t) hatFunction(t, 5000, onTime, offTime) + hatFunction(t, 6500, onTime, 0.1+onTime+offTime) + hatFunction(t, 8500, onTime, 0.2+2*onTime+offTime)
% };

duration = onTime + 2*offTime;
voltageFuns = {
	@(t) hatFunction(t, voltage, onTime, offTime)
 	@(t) 0
};

%% Plot
close all
plotVoltageFun(voltageFuns, duration)

%% Run
for i=1:1
     %data = trial(session, duration, voltageFuns, 'video', true);
    data = trial(session, duration, voltageFuns);
end

close all
plotData(data)