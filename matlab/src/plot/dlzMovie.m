% Main Parameters
outfile = 'dlzMovie.MP4';
dataFile = '1_A5_PID_SetPointTest.mat';

inFile = '1_A5_PID_SetPointTest_at6.0s.mp4';
plotWidth = 0.5; % In videoWidth units
plotZoom = 10;

tStart = 0;

% Load Video and Measurements
load(dataFile);
inVideo = VideoReader(inFile);
numFrames = round(inVideo.duration*inVideo.FrameRate);
framerate = inVideo.FrameRate;
numSkipFrames = round(framerate*tStart);

% Open output video
outVideo = VideoWriter(outfile);
outVideo.FrameRate = framerate;
open(outVideo)

% Skip some frame
fprintf('Skipping %i Frames\n', numSkipFrames)
for i=1:numSkipFrames
     inFrame = readFrame(inVideo);
end

% Render the video
disp('Rendering\n');
t = 0;
count = 1;
while hasFrame(inVideo)
    fprintf('Frame %i of %i\n', count,  round(numFrames))
    
    if(count==45)
        break
    end
    
    inFrame = readFrame(inVideo);
    inFrame = imrotate(inFrame, -90);
    
    % Set plot size
    plotSize = size(inFrame(:,:));
    plotSize(2) = round(plotWidth*plotSize(2));
    
    % Make new figuew
    close all            
    figure('Position', round([10, 10, plotSize(1), plotSize(2)]/plotZoom))
    
    % Plot
    labels = {};    
    labels{end+1} = 'Set Point';
    plot(data.time, data.hd);
    grid on
    hold on
    
    allheights = [data.hd; data.height];
    ylim([min(allheights), max(allheights)])
    
    labels{end+1} = 'Actuator';
    index = find(t<data.time, 1, 'first'); 
    plot(data.time(1:index), data.height(1:index))       
    
    xlabel('Time (s)')
    ylabel('Height (mm)')
    legend(labels)
    
    % Render Plot
    rendered = getframe(gcf);   
    rendered = imresize(rendered.cdata, plotSize);
    
    % Render to file
    outFrame = [inFrame, rendered];
    
%     close all
%     imshow(outFrame)
    
    writeVideo(outVideo, outFrame);
    
    %Increment Time
    t = t+1/framerate;
    count = count+1;
end

close(outVideo);