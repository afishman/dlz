function hd = hRampcase(hdi,t)

heights = [hdi, hdi, -24, -24, -29, -29, -19, -19, -22, -22, -34, -34, -14, -14, -20, -20, -34];
times = [0, 10, 13, 18, 20, 25, 28, 31, 32, 35, 39, 41, 47, 50, 52, 55, 60];
% heights = [hdi,hdi, -15, -15, -25, -25, -15, -15, -25, -25];
% times = [0, 10, 12.5, 20, 22.5, 30, 32.5, 40, 42.5, 50];

index = find(t<times, 1, 'first');

if isempty(index)
    hd = heights(end);
    return
end

h2 = heights(index);
t2 = times(index);

if index==1
    h1 = h2;
    t2 = t2;

else
    h1 = heights(index-1);
    t1 = times(index-1);
end

m = (h2-h1)/(t2-t1);
hd = h1+ m*(t-t1);


