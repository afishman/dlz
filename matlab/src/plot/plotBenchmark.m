function plotBenchmark(controller, data)
    b = benchmarks(controller, data);

    hold on
    xlabel('Time (s)')
    ylabel('Height (mm)')  
    
    labels = {};
    
    % Plot raw data
    linestyle = '--';
    
    plot(data.time, data.height)
    labels{end+1} = 'Actuator Response';
    
    vline(b.riseTime, ['b',linestyle])
    labels{end+1} = 'Rise Time';
    
    hline(b.overshoot+b.hd, ['k',linestyle]);
    labels{end+1} = 'Overshoot';
    
    hline(b.hd, ['r',linestyle]);
    labels{end+1} = 'Set Point';
    
    hline(b.hd+b.ssErr, ['m',linestyle])
    labels{end+1} = 'Steady State Error';
    
    vline(b.settleTime, ['g',linestyle])
    labels{end+1} = 'Settle Time';
    
    legend(labels); 
    
end