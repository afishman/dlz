function [x,y] = beamProfile(h, beam, numPts)

if ~exist('numPts')
    numPts = 100;
end

L = beam.L;
EI = beam.EI;

a = L/2;
b = a;

% Assume a profile of fixed beam with a point load
P = beamForce(h, beam);

x = linspace(0, L, numPts);
y = arrayfun(@(x)fixedBeamDeflection(x, P, L, EI), x);

