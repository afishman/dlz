close all

%% Parameters
beam = defaultBeam;
beam.V = 1000;

%% Steady state under no electromechanical loading
h = 10;
x = linspace(0, beam.L, 100);
plot(1000*x, 1000*deflection(beam, x, h));
grid on
title('Beam Profile')
xlabel('x (mm)')
ylabel('y (mm)')

%% Simulate
figure; hold on
title('Simulation')
xlabel('Time (s)')
ylabel('Height (mm)')
grid on

V = 0:50:500;
labels = {};
for i = 1:length(V)
    beam.V = V(i);
    
    options = odeset('Events', @eventsFun);
    sol = ode45(@(t,y)odeFun(t,y,beam), [0 10], [deflection(beam, beam.L),0], options);
    
    plot(sol.x, sol.y(1,:))
    labels{end+1} = sprintf('%.0fV', V(i));
end
ylim([0, 30e-3])
set(gca,'YDir','normal')
legend(labels)
