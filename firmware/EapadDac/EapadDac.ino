#include "cb101.h"

// Main Controls
float PERIOD = 8;
float PEAK_VOLTAGE = 10000;
float BASE_VOLTAGE = 0;

int LOOP_DELAY_MS = 30;

// For Voltage Feedback
int LED_PIN = 2;

int DAC1_ADDRESS = 0x62;
int VM1_PIN = A9;
int CM1_PIN = A10;
float bias1 = 0;

int DAC2_ADDRESS = 0x63;
int CM2_PIN = A3;
int VM2_PIN = A4;
float bias2 = 00;


Cb101* PSU1;
Cb101* PSU2;

// TODO Other DAC

void setup() {
    Serial.begin(115200);

    // Initialise CB101
    PSU1 = new Cb101(DAC1_ADDRESS, VM1_PIN, CM1_PIN);
    PSU2 = new Cb101(DAC2_ADDRESS, VM2_PIN, CM2_PIN);
}

void loop() 
{  
    // Update pattern
    // Relative voltage should be a number [0, 1] to indicate voltage of output 1 w.r.t
    //float relativeVoltage = 0.5;
    float relativeVoltage = sinTime(2);
    
    //sinTime(PERIOD);

    
    setLed(relativeVoltage);

    // Set output voltage
    PSU1->setHv(bias1 + BASE_VOLTAGE + relativeVoltage*(PEAK_VOLTAGE - BASE_VOLTAGE));
    PSU2->setHv(bias2 + BASE_VOLTAGE + (relativeVoltage)*(PEAK_VOLTAGE - BASE_VOLTAGE));

    // Print
    printVoltages();

    // Sleep
    delay(LOOP_DELAY_MS);
}

void printVoltages()
{
    String message = "";
    message += String(PSU1->readVoltageMonitor()) + ",";
    message += String(PSU2->readVoltageMonitor()) + ",";
    Serial.println(message);
}

// Returns a number 0->1, where 0 is BASE_VOLTAGE, 1 is PEAK_VOLTAGE
float sinTime(float period)
{
    float t = time_s();
    
    // Rescale to extents
    return 0.5*(sin(2.0*PI*t/period)+1);
}

// Returns a number 0->1, where 0 is BASE_VOLTAGE, 1 is PEAK_VOLTAGE
float squareTime(float period)
{
    float t = fmod(time_s(), period);
    return (t<period/2) ? 1 : 0;
}

// Time in seconds
float time_s()
{
    return ((float)millis())/1000;
}

// intensity should be between 0 (off) and 1 (full brightness)
void setLed(float intensity)
{    
    analogWrite(LED_PIN, 255*constrain(intensity, 0, 1));  
}
