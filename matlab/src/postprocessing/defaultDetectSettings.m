function s = defaultDetectSettings
    s = struct;
    s.lid_hue = [0.45, 0.55];
    s.lid_sat = [0, 1];
    s.lid_value = [0.8, 1];
    
    s.beam_hue = [0.92, 0.02];
    s.beam_sat = [0, 1];
    s.beam_value = [0.7, 1];
    
    s.lid_length_mm = 115;
end