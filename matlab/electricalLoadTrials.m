voltages = 2400:100:3000;

%% Hat Function
offTime = 60;
duration = offTime+10;


%% Plot

%% Run
for i=1:length(voltages)
    voltage= voltages(i);
    fprintf('%.0fV\n', voltage)   
    
    filename = sprintf('cantilever_%04.0fV', voltage);
    
    voltageFuns = {
        @(t) stepFunction(t, voltage, offTime)
        @(t) stepFunction(t, voltage, offTime)
    };

    close all
    plotVoltageFun(voltageFuns, duration)
    drawnow

    data = trial(session, duration, voltageFuns, 'filename', filename);
end
close all
plotData(data)