root = './A2/PID_P600_I60/';

dirNames = {
    'PID_15.18g_hd-05mm/', ...
    'PID_15.18g_hd-10mm/', ...
    'PID_15.18g_hd-15mm/', ...
    'PID_15.18g_hd-20mm/', ...
    'PID_15.18g_hd-25mm/', ...
    'PID_15.18g_hd-30mm/'
};

filenames = cell(1,0);

% Gather .mat files
for i=1:length(dirNames)
    dirName = [root, dirNames{i}];
    
    % List all .mat files
    matFiles = listFiles(dirName, '*.mat');
    
    % Extend filenames
    filenames = [filenames, matFiles];        
end

% Plot Each ones
for i = 1:length(filenames)
    filename = filenames{i};
    
    load(filename)
    plotData(data)
end
