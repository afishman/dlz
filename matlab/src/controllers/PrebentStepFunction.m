function voltage = PrebentStepFunction(t,onTime,offTime,voltageInput)
    period = onTime + offTime;
    tPhase = mod(t, period); % pri
    
    if tPhase < onTime
        voltage = voltageInput;
    else
        voltage = 0;
    end
end

