
// From CB-101 Datasheet
// https://www.xppower.com/portals/0/pdfs/SF_CB_Series.pdf
float HIGH_VOLTAGE_PSU_OUTPUT_VOLTS = 10000;
float HIGH_VOLTAGE_PSU_OUTPUT_AMPS =  0.001;

int VOLTAGE_MONITOR_PIN = A9;
int CURRENT_MONITOR_PIN = A10;
int VOLTAGE_CONTROL_PIN = 13;

// For Voltage Feedback
int LED_PIN = 2;

void setup(void) {
  Serial.begin(115200);
}

float time_s()
{
  return ((float)millis())/1000;
}

void loop() 
{  
      // Timekeeping
      float t = time_s();
      
      float voltage = HIGH_VOLTAGE_PSU_OUTPUT_VOLTS*0.5*(sin(2*PI*t)+1);
      voltage=5000;
      
      int pwm_level = (int) (255*voltage/HIGH_VOLTAGE_PSU_OUTPUT_VOLTS);
      analogWrite(LED_PIN, pwm_level);
      analogWrite(VOLTAGE_CONTROL_PIN, pwm_level);

      // Read Voltage Monitor
      float voltage_monitor = readVoltageMonitor();
      float current_monitor = readCurrentMonitor();
      
      // Print
      // Format: <t>, <height>, <voltage monitor>, <current monitor>, <set voltage>, <p>, <i>, <d>
      String message = "";
      message += String(t, 3);
      message += ",";
      message += String(voltage_monitor);
      message += ",";
      message += String(current_monitor);
      message += ",";
      message += String(voltage);

      Serial.println(message);

      delay(30);
}

float readVoltageMonitor()
{
    return (HIGH_VOLTAGE_PSU_OUTPUT_VOLTS/1023.0) * ((float)analogRead(VOLTAGE_MONITOR_PIN));
}

float readCurrentMonitor()
{
    return (HIGH_VOLTAGE_PSU_OUTPUT_AMPS/1023.0) * ((float)analogRead(CURRENT_MONITOR_PIN));
}

