clear all
close all

% https://www.makeitfrom.com/material-properties/SAE-AISI-1095-SUP4-1.1274-C100S-G10950-Carbon-Steel
thickness = 0.2e-3; % 50->200um;
width = 12.7e-3;

E = 190e9;
I = (1/3)*width*thickness^3;
G = 72e9;
L = (100)*1e-3;
k = 5/6;
A = thickness*width;
g = 9.8;

deflections = [15.2, 17.0, 18.85, 21.1, 22.3, 24.3, 25.4, 27.0];
baseMass = 5.3+3.77;
massAdded = 0:length(deflections)-1;
masses = baseMass + massAdded;

predictedDeflections = masses*g .* (L^3 / (E*I)); 

plot(masses, predictedDeflections, '-x');
hold on
grid on

plot(masses, deflections, '-x');
xlabel('mass (g)')
ylabel('deflection (mm)')
legend('Euler-Bernoulli Model', 'Experiment')
