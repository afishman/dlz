%% A simply supported beam of length L with a point load P
% Thanks: https://notendur.hi.is/thorstur/teaching/cont/Continuum_CommonBeamFormulas.pdf

P=1;
L=1;
a = L/2;
w=1;
E=1;
I=1;

% Calc Flexural Rigidity and b
EI = E*I;
b = L - a;

% Split Solution into two sections
xa = linspace(0, a, 100);
xb = linspace(b, L, 100);
ya = (P/(6*EI))*( b*xa.^3/L - (a*b*xa/L)*(2*L-a) );
yb = (P/(6*EI))*( b*xb.^3/L - (a*b*xb/L)*(2*L-a) -(xb-a).^3 );

% Merge together
x = [xa, xb];
y = [ya, yb];

plot(x,y)